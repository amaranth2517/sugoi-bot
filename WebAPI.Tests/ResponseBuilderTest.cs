using System;
using WebAPI.Extensions;
using Xunit;

namespace WebAPI.Tests
{
    public class ResponseBuilderTest
    {
        [Fact]
        public void Overflow()
        {
            var builder = new ResponseBuilder(150);
            builder.AppendLine("Amaranth's quest completions for the last 3 months:");
            builder.AppendLine("23.09.2018: Jewelry Points No screenshot.");
            builder.AppendLine("23.09.2018: Gear Points No screenshot.");
            builder.AppendLine("05.09.2018:  No screenshot.");

            var expected = "Amaranth's quest completions for the last 3 months:" + Environment.NewLine + "23.09.2018: Jewelry Points No screenshot." + Environment.NewLine +
                           "and another 2 record(-s)...";
            Assert.Equal(expected, builder.ToString());
        }
    }
}