using System.Collections.Generic;
using MongoDB.Bson;
using WebAPI.Models;

namespace WebAPI.Tests.Mocks
{
    internal static class Users
    {
        public static readonly User Current = new User {Username = "Current", Mention = "@Current", Id = 101};
        public static readonly User Player1 = new User {Username = "Player1", Mention = "@Player1", Id = 102};
        public static readonly User Player2 = new User {Username = "Player2", Mention = "@Player2", Id = 103};
        public static readonly User Player3 = new User {Username = "Player3", Mention = "@Player3", Id = 104};
    }

    internal static class Currencies
    {
        public static readonly Currency Coins = new Currency {Id = ObjectId.GenerateNewId(), Slug = "c", Description = "coin(-s)", Professions = new List<ulong>()};

        public static readonly Currency GearPoints = new Currency
            {Id = ObjectId.GenerateNewId(), Slug = "gp", Description = "gear point(-s)", Professions = new List<ulong> {Roles.Blacksmithing.Id, Roles.Woodworking.Id}};

        public static readonly Currency VesselPoints = new Currency {Id = ObjectId.GenerateNewId(), Slug = "vp", Description = "vessel point(-s)", Professions = new List<ulong> {Roles.Necromancy.Id}};

        public static readonly Currency JewelryPoints = new Currency
            {Id = ObjectId.GenerateNewId(), Slug = "jp", Description = "jewelry point(-s)", Professions = new List<ulong> {Roles.Jewelcrafting.Id}};
    }
}