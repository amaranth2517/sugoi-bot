using Discord;

namespace WebAPI.Tests.Mocks
{
    public static class Attachments
    {
        public static readonly IAttachment Screenshot = new Attachment {Url = "https://google.com"};
    }
}