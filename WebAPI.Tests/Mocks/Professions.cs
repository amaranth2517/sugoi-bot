namespace WebAPI.Tests.Mocks
{
    internal static class Roles
    {
        public static readonly Role Necromancy = new Role {Name = "Necromancy", Mention = "@Necromancy", Id = 1001};
        public static readonly Role Blacksmithing = new Role {Name = "Blacksmithing", Mention = "@Blacksmithing", Id = 1002};
        public static readonly Role Woodworking = new Role {Name = "Woodworking", Mention = "@Woodworking", Id = 1003};
        public static readonly Role Jewelcrafting = new Role {Name = "Jewelcrafting", Mention = "@Jewelcrafting", Id = 1004};
        public static readonly Role NonCrafting = new Role {Name = "NonCrafting", Mention = "@NonCrafting", Id = 99999};
        public static readonly Role Banker = new Role {Name = "Banker", Mention = "@Banker", Id = 99998};
        public static readonly Role Quester = new Role {Name = "Quester", Mention = "@Quester", Id = 99997};
    }
}