namespace WebAPI.Tests.Mocks
{
    public static class Ids
    {
        public static string One = "000000000000000000000001";
        public static string Two = "000000000000000000000002";
        public static string Three = "000000000000000000000003";
    }
}