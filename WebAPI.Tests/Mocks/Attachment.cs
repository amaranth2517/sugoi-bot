using Discord;

namespace WebAPI.Tests.Mocks
{
    public class Attachment : IAttachment
    {
        public ulong Id { get; }
        public string Filename { get; }
        public string Url { get; set; }
        public string ProxyUrl { get; }
        public int Size { get; }
        public int? Height { get; }
        public int? Width { get; }
    }
}