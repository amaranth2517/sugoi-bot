using MongoDB.Bson;

namespace WebAPI.Tests.Mocks
{
    public static class ObjectIds
    {
        public static ObjectId One = ObjectId.Parse(Ids.One);
        public static ObjectId Two = ObjectId.Parse(Ids.Two);
        public static ObjectId Three = ObjectId.Parse(Ids.Three);
    }
}