using System;
using System.Threading.Tasks;
using Discord;

namespace WebAPI.Tests.Mocks
{
    public class Role : IRole
    {
        public ulong Id { get; set; }
        public DateTimeOffset CreatedAt { get; }

        public Task DeleteAsync(RequestOptions options = null)
        {
            throw new NotImplementedException();
        }

        public string Mention { get; set; }

        public int CompareTo(IRole other)
        {
            throw new NotImplementedException();
        }

        public Task ModifyAsync(Action<RoleProperties> func, RequestOptions options = null)
        {
            throw new NotImplementedException();
        }

        public IGuild Guild { get; }
        public Color Color { get; }
        public bool IsHoisted { get; }
        public bool IsManaged { get; }
        public bool IsMentionable { get; }
        public string Name { get; set; }
        public GuildPermissions Permissions { get; }
        public int Position { get; }
    }
}