using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;
using WebAPI.Services;
using WebAPI.Tests.Mocks;
using Xunit;

namespace WebAPI.Tests
{
    public class QuestingServiceTest
    {
        private readonly QuestingService _service;
        private readonly MemoryRepository<Quest> _memoryQuests;
        private readonly MemoryRepository<Completion> _memoryCompletions;
        private readonly MemoryRepository<Reward> _memoryRewards;
        private readonly MemoryRepository<Rate> _memoryRates;
        private readonly MemoryRepository<Request> _memoryRequests;

        // todo: move
        public QuestingServiceTest()
        {
            var guildService = new Mock<IGuildService>();
            guildService.Setup(x => x.GetRole(Roles.Blacksmithing.Id)).Returns(() => Roles.Blacksmithing);
            guildService.Setup(x => x.GetRole(Roles.Woodworking.Id)).Returns(() => Roles.Woodworking);
            guildService.Setup(x => x.GetRole(Roles.Necromancy.Id)).Returns(() => Roles.Necromancy);

            guildService.Setup(x => x.GetUser(Users.Current.Id)).Returns(() => Users.Current);
            guildService.Setup(x => x.GetUser(Users.Player1.Id)).Returns(() => Users.Player1);
            guildService.Setup(x => x.GetUser(Users.Player2.Id)).Returns(() => Users.Player2);
            guildService.Setup(x => x.GetUser(Users.Player3.Id)).Returns(() => Users.Player3);

//            guildService.Setup(x => x.IsUserInRole(Users.Current, It.IsAny<ulong>())).Returns(() => true);
            guildService.Setup(x => x.IsUserInRole(Users.Player1, Roles.Quester.Id)).Returns(() => true);
            guildService.Setup(x => x.IsUserInRole(Users.Player2, Roles.Banker.Id)).Returns(() => true);

            _memoryQuests = new MemoryRepository<Quest>();
            _memoryCompletions = new MemoryRepository<Completion>();
            _memoryRewards = new MemoryRepository<Reward>();
            _memoryRates = new MemoryRepository<Rate>();
            _memoryRequests = new MemoryRepository<Request>();
            var mockSettings = new MemoryRepository<Setting>();
            mockSettings.Add(new Setting {Quester = Roles.Quester.Id, Banker = Roles.Banker.Id});
            var mockCurrencies = new MemoryRepository<Currency>();
            mockCurrencies.Add(Currencies.Coins);
            mockCurrencies.Add(Currencies.GearPoints);
            mockCurrencies.Add(Currencies.VesselPoints);
            mockCurrencies.Add(Currencies.JewelryPoints);

            _service = new QuestingService(guildService.Object, _memoryQuests, _memoryCompletions, _memoryRewards, _memoryRates, mockSettings, mockCurrencies, _memoryRequests);
        }

        [Fact]
        public void Quests()
        {
            Assert.Equal($"We have no active quests.{Environment.NewLine}", _service.Quests(null).Message);

            var quest = AddTestQuest();

            _memoryQuests.Add(new Quest {Active = false});

            var response = _service.Quests(null);
            var builder = new StringBuilder();
            builder.AppendLine($"**{quest.QuestType.Print()}**");
            builder.AppendLine($"\"{quest.Name}\"");
            builder.AppendLine($"{quest.Description}");
            builder.AppendLine($"Reward: {quest.Amount:0.##} {Currencies.Coins.Description}");
            builder.AppendLine();
            Assert.Equal(builder.ToString(), response.Message);

            response = _service.Quests("show-ids");
            builder = new StringBuilder();
            builder.AppendLine($"**{quest.QuestType.Print()}**");
            builder.AppendLine($"\"{quest.Name}\" ({quest.Id}, {quest.Slug})");
            builder.AppendLine($"{quest.Description}");
            builder.AppendLine($"Reward: {quest.Amount:0.##} {Currencies.Coins.Description}");
            builder.AppendLine();
            Assert.Equal(builder.ToString(), response.Message);
        }

        private Quest AddTestQuest()
        {
            var quest = new Quest
            {
                Id = ObjectIds.One,
                Description = "Kill 100 enemies",
                Active = true,
                Amount = 3000,
                Name = "Kill them all",
                QuestType = QuestType.Pvp,
                Slug = "kta",
                CurrencyId = Currencies.Coins.Id,
                Version = 2
            };
            _memoryQuests.Add(quest);
            return quest;
        }

        [Fact]
        public void AddQuest()
        {
            var command = "-a 3000 -c c -n Kill them all -d Kill 1000 enemies -t pvp".Split(" ");

            var context = new ModuleContext(Users.Current);
            Assert.Equal(Responses.Unauthorized, _service.AddQuest(context, command));

            context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.QuestAdded, _service.AddQuest(context, command));
            var quest = _memoryQuests.GetBySlug("kta");
            Assert.NotNull(quest);
            Assert.Equal("Kill them all", quest.Name);

            // Add quest with the same slug. Say the another quest shares this slug and pick different name.
            command = "-a 3000 -c c -n Kick the adventure -d Visit 4 campaign worlds -t pvp".Split(" ");
            context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.DuplicateSlug, _service.AddQuest(context, command));

            command = "-c c -n Some failing quest -d Kill 1000 enemies -t pvp".Split(" ");
            context = new ModuleContext(Users.Player1);
            // todo: reports duplicate quest
            // Assert.Equal(Responses.AmountIsNotNumber, _service.AddQuest(context, command));
        }

        [Fact]
        public void UpdateQuest()
        {
            var command = "kta -a 5000".Split(" ");

            var context = new ModuleContext(Users.Current);
            Assert.Equal(Responses.Unauthorized, _service.UpdateQuest(context, command));

            var quest = AddTestQuest();
            context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.QuestUpdated, _service.UpdateQuest(context, command));
            Assert.Equal(5000, quest.Amount);
        }

        [Fact]
        public void EnableQuest()
        {
            var quest = AddTestQuest();
            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.QuestEnabled, _service.EnableQuest(context, quest.Slug));
        }

        [Fact]
        public void DisableQuest()
        {
            var quest = AddTestQuest();
            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.QuestDisabled, _service.DisableQuest(context, quest.Slug));
        }

        [Fact]
        public void AddTitle()
        {
            var quest = AddTestQuest();
            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.TitleAdded, _service.AddTitle(context, quest.Slug, 5, "I", "am", "the", "king"));
            Assert.Equal("I am the king", quest.Titles.First().Name);
        }

        [Fact]
        public void Completions()
        {
            var quest = AddTestQuest();
            var completion = AddTestCompletion(quest);

            var context = new ModuleContext(Users.Player1);
            var builder = new StringBuilder();
            builder.AppendLine($"{Users.Player1.GetUsername()}'s quest completions:");
            builder.AppendLine($"{completion.CreatedAt.ToShortDateString()}: {completion.Amount} {Currencies.GearPoints.Description}, {quest.Name} No screenshot.");
            Assert.Equal(builder.ToString(), _service.Completions(context, null).Message);

            builder = new StringBuilder();
            builder.AppendLine($"{quest.Name} completions:");
            builder.AppendLine($"{completion.CreatedAt.ToShortDateString()}: {completion.Amount} {Currencies.GearPoints.Description}, {Users.Player1.GetUsername()}. No screenshot.");
            Assert.Equal(builder.ToString(), _service.Completions(context, quest.Slug).Message);
        }

        private Completion AddTestCompletion(Quest quest)
        {
            var completion = new Completion
            {
                PlayerId = Users.Player1.Id,
                Amount = 3,
                CurrencyId = Currencies.GearPoints.Id,
                QuestId = quest.Id,
                HelpersIds = new List<ulong>(),
                Version = 2
            };
            _memoryCompletions.Add(completion);
            return completion;
        }

        [Fact]
        public void Rewards()
        {
            var mentionedUser = Users.Player1;
            var reward = new Reward
            {
                PlayerId = mentionedUser.Id,
                Coins = 300,
                GearPoints = 4.5f,
                VesselPoints = 2,
                JewelryPoints = 4.3f
            };
            _memoryRewards.Add(reward);

            var context = new ModuleContext(Users.Player2);
            Assert.Equal(Responses.NoRewards, _service.Rewards(context, null));

            context = new ModuleContext(Users.Current, mentionedUser);
            var expected =
                $"{mentionedUser.GetUsername()} has {reward.GearPoints} gear point(-s); {reward.VesselPoints} vessel point(-s); {reward.JewelryPoints} jewelry point(-s); {reward.Coins} coin(-s);";
            Assert.Equal(expected, _service.Rewards(context, mentionedUser.GetUsername()).Message);

            reward.GearPoints = -3f;
            reward.VesselPoints = -0.00000000003f;
            reward.Coins = -100f;

            expected = $"{mentionedUser.GetUsername()} has -3 gear point(-s); {4.3f} jewelry point(-s); -100 coin(-s);";
            Assert.Equal(expected, _service.Rewards(context, mentionedUser.GetUsername()).Message);
        }

        [Fact]
        public void Titles()
        {
            var quest = AddTestQuest();
            var title = new Title {Completions = 3, Name = "Working!"};
            quest.Titles = new List<Title> {title};
            AddTestCompletion(quest);

            var context = new ModuleContext(Users.Player1);
            Assert.Equal(title.Name, _service.Titles(context, null).Message);
        }

        [Fact]
        public void Turn()
        {
            var quest = AddTestQuest();

            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.Unauthorized, _service.Turn(context, quest.Slug, null, null));

            context = new ModuleContext(Users.Player2);
            Assert.Equal(Responses.MissingPlayers, _service.Turn(context, quest.Slug, null, null));

            var mentionedUser = Users.Player3;
            context = new ModuleContext(Users.Player2, mentionedUser);
            Assert.Equal(Responses.TurnedIn, _service.Turn(context, quest.Slug, null, null));
            var completion = _memoryCompletions.GetByPlayer(mentionedUser.Id).First();
            Assert.Equal(quest.Amount, completion.Amount);
            Assert.Equal(quest.CurrencyId, completion.CurrencyId);
            var reward = _memoryRewards.GetByPlayer(mentionedUser.Id).First();
            Assert.Equal(quest.Amount, reward.Points[completion.CurrencyId.ToString()]);
        }

        [Fact]
        public void Rates()
        {
            _memoryRates.Add(new Rate
            {
                SourceAmount = 1,
                SourceCurrencyId = Currencies.GearPoints.Id,
                TargetAmount = 10,
                TargetCurrencyId = Currencies.JewelryPoints.Id,
                Version = 2
            });
            _memoryRates.Add(new Rate
            {
                SourceAmount = 1,
                SourceCurrencyId = Currencies.VesselPoints.Id,
                TargetAmount = 60000,
                TargetCurrencyId = Currencies.Coins.Id,
                Version = 2
            });

            var context = new ModuleContext(Users.Player2);
            var response = new StringBuilder();
            response.AppendLine("1 gear point(-s) = 10 jewelry point(-s)");
            response.AppendLine("1 vessel point(-s) = 60000 coin(-s)");
            Assert.Equal(response.ToString(), _service.Rates(context).Message);
        }

        [Fact]
        public void Rate()
        {
            Assert.Empty(_memoryRates.GetAll());

            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.Unauthorized, _service.Rate(context, 1, "gp", 500, "c"));

            context = new ModuleContext(Users.Player2);
            Assert.Equal(Responses.RateSet, _service.Rate(context, 1, "gp", 500, "c"));

            context = new ModuleContext(Users.Player2);
            Assert.Equal(Responses.RateSet, _service.Rate(context, 1, "gp", 1000, "c"));

            Assert.Single(_memoryRates.GetAll());
        }

        [Fact]
        public void Convert()
        {
            var reward = new Reward
            {
                PlayerId = Users.Player2.Id,
                Points = new Dictionary<string, float>
                {
                    {Currencies.GearPoints.Id.ToString(), 10},
                    {Currencies.VesselPoints.Id.ToString(), 2},
                    {Currencies.JewelryPoints.Id.ToString(), 4},
                    {Currencies.Coins.Id.ToString(), 3000}
                },
                Version = 2
            };
            _memoryRewards.Add(reward);

            var context = new ModuleContext(Users.Player2);
            Assert.Equal(Responses.ExchangeNotAllowed, _service.Convert(context, "4.5", "gp", "jp"));

            Assert.Equal(Responses.RateSet, _service.Rate(new ModuleContext(Users.Player2), 1, "gp", 10, "jp"));

            Assert.Equal($"{4.5f} gear point(-s) got converted to 45 jewelry point(-s).", _service.Convert(context, "4.5", "gp", "jp").Message);

            Assert.Equal(Responses.Unauthorized, _service.Convert(context, "-1", "gp", "jp"));
            Assert.Equal(Responses.Unauthorized, _service.Convert(context, "0", "gp", "jp"));
        }

        [Fact]
        public void Total()
        {
            var reward = new Reward
            {
                PlayerId = Users.Player1.Id,
                Points = new Dictionary<string, float>
                {
                    {Currencies.GearPoints.Id.ToString(), 10},
                    {Currencies.VesselPoints.Id.ToString(), 5},
                    {Currencies.JewelryPoints.Id.ToString(), 2},
                    {Currencies.Coins.Id.ToString(), 3000}
                },
                Version = 2
            };
            _memoryRewards.Add(reward);
            reward = new Reward
            {
                PlayerId = Users.Player1.Id,
                Points = new Dictionary<string, float>
                {
                    {Currencies.GearPoints.Id.ToString(), 10},
                    {Currencies.VesselPoints.Id.ToString(), 5},
                    {Currencies.JewelryPoints.Id.ToString(), 2},
                    {Currencies.Coins.Id.ToString(), 3000}
                },
                Version = 2
            };
            _memoryRewards.Add(reward);
            var request = new Request
            {
                Id = ObjectIds.One,
                PlayerId = Users.Player1.Id,
                CrafterId = 0,
                Description = "Dagger",
                ProfessionId = Roles.Blacksmithing.Id,
                State = CraftingState.Requested,
                Amount = 5,
                CurrencyId = Currencies.GearPoints.Id
            };
            _memoryRequests.Add(request);


            var context = new ModuleContext(Users.Player2);
            var total = $"25 {Currencies.GearPoints.Description}, 10 {Currencies.VesselPoints.Description}, 4 {Currencies.JewelryPoints.Description}, 6000 {Currencies.Coins.Description}";
            Assert.Equal(total, _service.Total(context).Message);
        }
    }
}