using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Discord;
using Moq;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;
using WebAPI.Services;
using WebAPI.Tests.Mocks;
using Xunit;

namespace WebAPI.Tests
{
    public class CraftingServiceTest
    {
        private readonly CraftingService _service;
        private readonly MemoryRepository<Request> _memoryRequests;
        private readonly MemoryRepository<Reward> _memoryRewards;
        private readonly Mock<IGuildService> _guildService;

        public CraftingServiceTest()
        {
            _guildService = new Mock<IGuildService>();
            _guildService.Setup(x => x.GetRole(Roles.Blacksmithing.Id)).Returns(() => Roles.Blacksmithing);
            _guildService.Setup(x => x.GetRole(Roles.Woodworking.Id)).Returns(() => Roles.Woodworking);
            _guildService.Setup(x => x.GetRole(Roles.Necromancy.Id)).Returns(() => Roles.Necromancy);

            _guildService.Setup(x => x.GetUser(Users.Current.Id)).Returns(() => Users.Current);
            _guildService.Setup(x => x.GetUser(Users.Player1.Id)).Returns(() => Users.Player1);
            _guildService.Setup(x => x.GetUser(Users.Player2.Id)).Returns(() => Users.Player2);
            _guildService.Setup(x => x.GetUser(Users.Player3.Id)).Returns(() => Users.Player3);

            _guildService.Setup(x => x.IsUserInRole(Users.Current, It.IsAny<ulong>())).Returns(() => true);

            _memoryRewards = new MemoryRepository<Reward>();
            _memoryRequests = new MemoryRepository<Request>();
            var mockSettings = new MemoryRepository<Setting>();
            mockSettings.Add(new Setting {Professions = new List<ulong> {Roles.Blacksmithing.Id, Roles.Jewelcrafting.Id, Roles.Woodworking.Id, Roles.Necromancy.Id}});
            var mockCurrencies = new MemoryRepository<Currency>();
            mockCurrencies.Add(Currencies.Coins);
            mockCurrencies.Add(Currencies.GearPoints);
            mockCurrencies.Add(Currencies.VesselPoints);
            mockCurrencies.Add(Currencies.JewelryPoints);

            _service = new CraftingService(_guildService.Object, _memoryRewards, _memoryRequests, mockSettings, mockCurrencies);
        }

        public static IEnumerable<object[]> RequestInput => new[]
        {
            new object[] {10, 0, 3, Roles.Blacksmithing, 9, 0, 3, Responses.RequestCreated},
            new object[] {5, 0, 3, Roles.Woodworking, 4, 0, 3, Responses.RequestCreated},
            new object[] {0, 5, 3, Roles.Necromancy, 0, 4, 3, Responses.RequestCreated},
            new object[] {5, 0, 3, Roles.Jewelcrafting, 5, 0, 2, Responses.RequestCreated},
            new object[] {0, 5, 3, Roles.Blacksmithing, 0, 5, 3, Responses.NotEnoughPoints},
            new object[] {5, 0, 3, Roles.Necromancy, 5, 0, 3, Responses.NotEnoughPoints},
            new object[] {5, 0, 0, Roles.Jewelcrafting, 5, 0, 0, Responses.NotEnoughPoints},
            new object[] {5, 0, 0, Roles.NonCrafting, 5, 0, 0, Responses.Unauthorized}
        };

        [Theory, MemberData(nameof(RequestInput))]
        public void Request(int gp, int vp, int jp, Role role, int resultGP, int resultVP, int resultJP, Response response)
        {
            var context = new ModuleContext(Users.Current);
            Assert.Equal(Responses.MissingProfession, _service.Request(context, "Description"));

            context = new ModuleContext(Users.Current, Roles.Blacksmithing);
            Assert.Equal(Responses.MissingDescription, _service.Request(context, Roles.Blacksmithing.Name));

            var reward = new Reward
            {
                Version = 2,
                Points = new Dictionary<string, float>
                {
                    {Currencies.GearPoints.Id.ToString(), gp},
                    {Currencies.VesselPoints.Id.ToString(), vp},
                    {Currencies.JewelryPoints.Id.ToString(), jp}
                },
                PlayerId = Users.Current.Id
            };
            _memoryRewards.Add(reward);
            context = new ModuleContext(Users.Current, role);
            Assert.Equal(response, _service.Request(context, role.Name, "I", "need", "dagger"));
            Assert.Equal(resultGP, reward.Points[Currencies.GearPoints.Id.ToString()]);
            Assert.Equal(resultVP, reward.Points[Currencies.VesselPoints.Id.ToString()]);
            Assert.Equal(resultJP, reward.Points[Currencies.JewelryPoints.Id.ToString()]);

            if (!Equals(response, Responses.RequestCreated))
                return;

            var request = _memoryRequests.GetAll().FirstOrDefault();
            Assert.NotNull(request);
            Assert.Equal("I need dagger", request.Description);

            reward.Points[Currencies.GearPoints.Id.ToString()] = 10;
            context = new ModuleContext(Users.Current, Roles.Blacksmithing);
            Assert.Equal(Responses.RequestCreated.Params(request.Id, 3, Currencies.GearPoints.Description).Message,
                _service.Request(context, Roles.Blacksmithing.Name, "3", "good", "daggers").Message);
            Assert.Equal(7, reward.Points[Currencies.GearPoints.Id.ToString()]);
            request = _memoryRequests.GetAll().LastOrDefault();
            Assert.NotNull(request);
            Assert.Equal(3, request.Amount);
            Assert.Equal("good daggers", request.Description);

            Assert.Equal(Responses.AmountIsNegative, _service.Request(context, Roles.Blacksmithing.Name, "-3", "good", "daggers"));
        }

        [Fact]
        public void Requests()
        {
            var profession = Roles.Blacksmithing;
            var context = new ModuleContext(Users.Current);
            Assert.Equal($"**Nothing is {CraftingState.Requested.Print()}.**{Environment.NewLine}**Nothing is {CraftingState.Crafted.Print()}.**{Environment.NewLine}{Environment.NewLine}",
                _service.Requests(context).Message);

            var request = new Request
            {
                Id = ObjectIds.One,
                PlayerId = Users.Player1.Id,
                CrafterId = 0,
                Description = "Dagger",
                ProfessionId = profession.Id,
                State = CraftingState.Requested,
                Amount = 1,
                CurrencyId = Currencies.GearPoints.Id
            };
            _memoryRequests.Add(request);
            var secondRequest = new Request
            {
                Id = ObjectIds.Two,
                PlayerId = Users.Player2.Id,
                CrafterId = Users.Player2.Id,
                Description = "Sword",
                ProfessionId = profession.Id,
                State = CraftingState.Crafting,
                Amount = 2,
                CurrencyId = Currencies.GearPoints.Id
            };
            _memoryRequests.Add(secondRequest);
            context = new ModuleContext(Users.Current, profession, Users.Player1);
            var response = _service.Requests(context);
            var builder = new StringBuilder();
            builder.AppendLine($"**{Users.Player1.GetUsername()}'s {CraftingState.Requested.Print()} items:**");
            builder.AppendLine($"{request.CreatedAt.ToShortDateString()}: {request.Amount} {Currencies.GearPoints.Description}, *{request.Description}* ({ObjectIds.One}, {profession.Name})");
            builder.AppendLine();
            builder.AppendLine($"**Nothing is {CraftingState.Crafted.Print()}.**");
            builder.AppendLine();
            builder.AppendLine($"**{profession.Name} {CraftingState.Requested.Print()} items:**");
            builder.AppendLine(
                $"{request.CreatedAt.ToShortDateString()}: {request.Amount} {Currencies.GearPoints.Description}, *{request.Description}* ({ObjectIds.One} by {Users.Player1.GetUsername()}).");
            builder.AppendLine();
            builder.AppendLine($"**{profession.Name} {CraftingState.Crafting.Print()} items:**");
            builder.AppendLine(
                $"{secondRequest.CreatedAt.ToShortDateString()}: {secondRequest.Amount} {Currencies.GearPoints.Description}, *{secondRequest.Description}* ({ObjectIds.Two} by {Users.Player2.GetUsername()}, crafter: {Users.Player2.GetUsername()}).");
            builder.AppendLine();
            Assert.Equal(builder.ToString(), response.Message);

            context = new ModuleContext(Users.Current, profession);
            response = _service.Requests(context);
            builder = new StringBuilder();
            builder.AppendLine($"**{profession.Name} {CraftingState.Requested.Print()} items:**");
            builder.AppendLine(
                $"{request.CreatedAt.ToShortDateString()}: {request.Amount} {Currencies.GearPoints.Description}, *{request.Description}* ({ObjectIds.One} by {Users.Player1.GetUsername()}).");
            builder.AppendLine();
            builder.AppendLine($"**{profession.Name} {CraftingState.Crafting.Print()} items:**");
            builder.AppendLine(
                $"{secondRequest.CreatedAt.ToShortDateString()}: {secondRequest.Amount} {Currencies.GearPoints.Description}, *{secondRequest.Description}* ({ObjectIds.Two} by {Users.Player2.GetUsername()}, crafter: {Users.Player2.GetUsername()}).");
            builder.AppendLine();
            Assert.Equal(builder.ToString(), response.Message);
        }

        [Fact]
        public void Crafting()
        {
            var context = new ModuleContext(Users.Current);

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.One,
                PlayerId = Users.Player1.Id,
                Description = "Dagger",
                ProfessionId = Roles.Blacksmithing.Id,
                State = CraftingState.Requested
            });
            Assert.Equal(Responses.WillCraft, _service.Crafting(context, Ids.One));

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.Two,
                PlayerId = Users.Player2.Id,
                Description = "Staff",
                ProfessionId = Roles.Woodworking.Id,
                State = CraftingState.Crafting,
                CrafterId = Users.Player1.Id
            });
            Assert.Equal(Responses.AlreadyCrafting, _service.Crafting(context, Ids.Two));

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.Three,
                PlayerId = Users.Player3.Id,
                Description = "Sword",
                ProfessionId = Roles.Blacksmithing.Id,
                State = CraftingState.Crafted,
                CrafterId = Users.Player1.Id
            });
            Assert.Equal(Responses.AlreadyCrafting, _service.Crafting(context, Ids.Three));
        }

        [Fact]
        public void Crafted()
        {
            var screenshot = Attachments.Screenshot;
            var context = new ModuleContext(Users.Current, screenshot);

            var request = new Request
            {
                Id = ObjectIds.One,
                PlayerId = Users.Player1.Id,
                Description = "Dagger",
                ProfessionId = Roles.Blacksmithing.Id,
                State = CraftingState.Crafting,
                CrafterId = Users.Current.Id
            };
            _memoryRequests.Add(request);
            Assert.Equal(Responses.Crafted, _service.Crafted(context, Ids.One));
            Assert.Equal(screenshot.Url, request.Screenshot);

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.Two,
                PlayerId = Users.Player2.Id,
                Description = "Staff",
                ProfessionId = Roles.Woodworking.Id,
                State = CraftingState.Crafting,
                CrafterId = Users.Player1.Id
            });
            Assert.Equal(Responses.Unauthorized, _service.Crafted(context, Ids.Two));

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.Three,
                PlayerId = Users.Player2.Id,
                Description = "Staff",
                ProfessionId = Roles.Woodworking.Id,
                State = CraftingState.Crafted,
                CrafterId = Users.Current.Id
            });
            Assert.Equal(Responses.NotCrafting, _service.Crafted(context, Ids.Three));

            context = new ModuleContext(Users.Current, Roles.Necromancy);
            Assert.Equal("Current crafted 0 item(-s) with Necromancy.", _service.Crafted(context, Roles.Necromancy.Name).Message);

            context = new ModuleContext(Users.Current, Roles.Blacksmithing);
            Assert.Equal("Current crafted 1 item(-s) with Blacksmithing.", _service.Crafted(context, Roles.Blacksmithing.Name).Message);

            context = new ModuleContext(Users.Current, Roles.Woodworking);
            Assert.Equal("Current crafted 1 item(-s) with Woodworking.", _service.Crafted(context, Roles.Woodworking.Name).Message);

            _memoryRequests.Add(new Request
            {
                Id = ObjectIds.Three,
                PlayerId = Users.Player2.Id,
                Description = "Testing amount",
                ProfessionId = Roles.Woodworking.Id,
                State = CraftingState.Crafted,
                CrafterId = Users.Current.Id,
                Amount = 5
            });

            context = new ModuleContext(Users.Current, Roles.Woodworking);
            Assert.Equal("Current crafted 6 item(-s) with Woodworking.", _service.Crafted(context, Roles.Woodworking.Name).Message);
        }

        [Fact]
        public void Profession()
        {
            var context = new ModuleContext(Users.Current, Roles.Necromancy);

            _guildService.Setup(x => x.GetUsersInRole(It.IsAny<ulong>())).Returns(() => new List<IUser> {Users.Current});
            Assert.Equal(Responses.Removed, _service.Profession(context));

            _guildService.Setup(x => x.GetUsersInRole(It.IsAny<ulong>())).Returns(() => new List<IUser>());
            Assert.Equal(Responses.Assigned, _service.Profession(context));
        }
    }
}