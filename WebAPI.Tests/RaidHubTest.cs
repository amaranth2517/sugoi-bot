using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using NLog;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Targets;
using NLog.Web;
using WebAPI.Database;
using WebAPI.Hubs;
using Xunit;
using Xunit.Abstractions;

namespace WebAPI.Tests
{
    public class RaidHubTest : IDisposable
    {
        private readonly RaidHub _hub;
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly MemoryTarget _memoryTarget;

        public RaidHubTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;

            var configuration = new LoggingConfiguration();
            _memoryTarget = new MemoryTarget {Name = "mem"};
            configuration.AddTarget(_memoryTarget);
            configuration.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, _memoryTarget));
            LogManager.Configuration = configuration;

            // _hub = new RaidHub(new MemoryRepository<Raid>(), new NLogLoggerProvider(NLogAspNetCoreOptions.Default, LogManager.LogFactory).CreateLogger("test"));
            _hub = new RaidHub(new MemoryRepository<Raid>(), new NullLogger<RaidHub>());

            var context = new Mock<HubCallerContext>();
            context.Setup(x => x.ConnectionId).Returns("sdfgnsduhfiusd73423");
            _hub.Context = context.Object;

            var client = new Mock<IClientProxy>();
            client.Setup(x => x.SendCoreAsync(It.IsAny<string>(), new object[0], new CancellationToken()));

            var clients = new Mock<IHubCallerClients>();
            clients.Setup(x => x.Caller).Returns(client.Object);
            clients.Setup(x => x.Clients(It.IsAny<List<string>>())).Returns(client.Object);
            clients.Setup(x => x.Client(It.IsAny<string>())).Returns(client.Object);

            _hub.Clients = clients.Object;
        }

        // Checks if multiple clients create raid with the same handle at the same time.
        [Fact]
        public async Task JoinTest()
        {
            var tasks = new List<Task>();

            for (var i = 0; i < 254; i++)
            for (var j = 0; j < 5; j++)
                tasks.Add(_hub.Join("testRaid", new IPEndPoint(IPAddress.Parse($"83.23.{j}.{i}"), 2423 + i), $"TestCharacter{i + 1}"));

            await Task.WhenAll(tasks);

            Assert.Equal(1, _hub.GetRaidsCount());
        }

        public void Dispose()
        {
            var logs = _memoryTarget.Logs;
            foreach (var log in logs)
            {
                _testOutputHelper.WriteLine(log);
            }
        }
    }
}