using System;
using System.Collections.Generic;
using Moq;
using WebAPI.Database;
using WebAPI.Models;
using WebAPI.Services;
using WebAPI.Tests.Mocks;
using Xunit;

namespace WebAPI.Tests
{
    public class AdminServiceTest
    {
        private readonly AdminService _service;
        private readonly MemoryRepository<Request> _memoryRequests;
        private readonly MemoryRepository<Reward> _memoryRewards;
        private readonly MemoryRepository<Completion> _memoryCompletions;
        private readonly Mock<IGuildService> _guildService;
        private MemoryRepository<Setting> _memorySettings;

        public AdminServiceTest()
        {
            _guildService = new Mock<IGuildService>();
            _guildService.Setup(x => x.IsUserAdmin(Users.Current.Id)).Returns(() => true);

            _memoryRewards = new MemoryRepository<Reward>();
            _memoryRequests = new MemoryRepository<Request>();
            _memoryCompletions = new MemoryRepository<Completion>();

            _memorySettings = new MemoryRepository<Setting>();
            _memorySettings.Add(new Setting {Professions = new List<ulong> {Roles.Blacksmithing.Id, Roles.Jewelcrafting.Id, Roles.Woodworking.Id, Roles.Necromancy.Id}});
            var mockCurrencies = new MemoryRepository<Currency>();
            mockCurrencies.Add(Currencies.Coins);
            mockCurrencies.Add(Currencies.GearPoints);
            mockCurrencies.Add(Currencies.VesselPoints);
            mockCurrencies.Add(Currencies.JewelryPoints);

            var databaseService = new Mock<IDatabaseService>();
            databaseService.Setup(x => x.DropCollection(It.IsAny<string>()));

            _service = new AdminService(_guildService.Object, _memorySettings, mockCurrencies, databaseService.Object);
        }

        [Fact]
        public void WipeDBTest()
        {
            var context = new ModuleContext(Users.Player1);
            Assert.Equal(Responses.Unauthorized, _service.WipeDB(context, null, DateTime.Now));

            context = new ModuleContext(Users.Current);
            Assert.Equal(Responses.WriteTokenToConfirm, _service.WipeDB(context, null, DateTime.Now));

            var settings = _memorySettings.FirstOrDefault();
            Assert.NotNull(settings);
            Assert.Equal(Responses.Unauthorized, _service.WipeDB(context, "wrong_token", DateTime.Now));

            Assert.Equal(Responses.WriteTokenToConfirm, _service.WipeDB(context, settings.WipeToken, DateTime.Now.AddMinutes(3)));

            Assert.Equal(Responses.DatabaseWiped, _service.WipeDB(context, settings.WipeToken, DateTime.Now));
            Assert.Null(settings.WipeToken);
        }
    }
}