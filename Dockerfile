FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine3.12 AS builder

WORKDIR /source

COPY ./*.sln ./
COPY ./WebAPI/*.csproj ./WebAPI/
COPY ./WebAPI.Tests/*.csproj ./WebAPI.Tests/
RUN dotnet restore

COPY . ./

RUN dotnet test WebAPI.Tests
RUN dotnet publish WebAPI --output /app/ --configuration Release

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine3.12
WORKDIR /app
COPY --from=builder /app .
RUN apk add bash
RUN apk add mongodb-tools
CMD ["dotnet", "WebAPI.dll"] 