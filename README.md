# sugoi-bot

### Requirements
1. dotnet core v2.1 (C#)
2. internet connection to mongodb and discord api
3. it is good to have knowledge of:
   * design patterns
   * devops (CI/docker/heroku) 
   * TDD
   * NoSQL (MongoDB)
   
### Installation and Running
1. Install [docker](https://docs.docker.com/install/) and start it.
2. Make sure 80 port is free.
3. In console run: 

    docker build -t local .
    && docker run   
    -p 80:80    
    --env ASPNETCORE_ENVIRONMENT=Development   
    --env SECRET={bot api key}   
    --env CONNECTION_STRING={mongodb connection string}   
    --env BANKER_ID={list of bankers ids}   
    --env QUESTER_ID={list of questers ids}   
    --env DEV_GUILD_ID={testing discord server}   
    --env LIVE_GUILD_ID={production discord server}   
    --env VESSELS_PROFESSIONS={list of vessel professions}   
    --env GEAR_PROFESSIONS={list of gear professions}   
    --name sugoi-bot   
    local

   *For Amatalk discord settings ask Amaranth.*    
   *If docker is not option, you can install dotnet core on your machine.*
4. Check running container on http://locahost.
5. Bot will report "Connected" in console once it successfully connects to the discord api.

###  TDD
WebAPI.Tests contain mocks for database and discord client and unit tests for services commands.

You can write failing unit test before you add new command/service/module. Then make it pass test.

TDD allows to develop and test everything without being connected to discord api or real database at all.


### Design
For mocking and decoupling purposes database layer is abstracted with IRepository pattern.

Commands are decomposed to modules and service methods. Module purpose is to validate number of parameteres, set attrubutes and delegate flow to service.
Service contains all business logic and returns Response object, which contains code and message.
This allows to unix test only services and let everything else run on abstractions.

DI is everywhere. It allows to easily switch dependencies.

Application is using Hangfire to schedule automatic backups for MongoDB.

MVC is setup, but not used now. Discord bot runs in separate process.

### Deployment
Application is deployed by bitbucket pipeline after merge into master by building and pushing Docker image to heroku container hub.

It runs on heroku.