﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using FluentScheduler;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using WebAPI.Converters;
using WebAPI.Database;
using WebAPI.Helpers;
using WebAPI.Hubs;
using WebAPI.Models;
using WebAPI.Services;
using Request = WebAPI.Models.Request;

namespace WebAPI
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private const string RaidCollection = "CrowfallParser";

        private static DateTime _started;
        // private static Timer _timer;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // ReSharper disable once UnusedMember.Global
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSignalR(builder =>
                {
                    builder.KeepAliveInterval = TimeSpan.FromSeconds(2);
                    builder.ClientTimeoutInterval = TimeSpan.FromSeconds(10);
                    builder.MaximumReceiveMessageSize = 32 * 1024 * 10;
                })
                .AddJsonProtocol(builder =>
                {
                    builder.PayloadSerializerOptions.Converters.Add(new IPAddressConverter());
                    builder.PayloadSerializerOptions.Converters.Add(new IPEndPointConverter());
                });

            var settingsService = new SettingsService();
            var mongoContext = new MongoContext(settingsService);

            services.AddSingleton(new DiscordSocketClient())
                .AddSingleton<ISettingsService>(settingsService)
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<IMailService>(new SendGridService(settingsService))
                .AddSingleton(mongoContext)
                .AddTransient<CraftingService>()
                .AddTransient<QuestingService>()
                .AddTransient<Func<SocketGuild, IGuildService>>(provider => guild => new GuildService(_env, guild, settingsService, LogManager.GetCurrentClassLogger()))
                .AddTransient<Func<string, IRepository<Setting>>>(provider => dbName => new SettingRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Reward>>>(provider => dbName => new RewardRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Request>>>(provider => dbName => new RequestRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Completion>>>(provider => dbName => new CompletionRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Quest>>>(provider => dbName => new QuestRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Rate>>>(provider => dbName => new RateRepository(mongoContext, dbName))
                .AddTransient<Func<string, IRepository<Currency>>>(provider => dbName => new CurrencyRepository(mongoContext, dbName))
                .AddTransient<Func<string, IDatabaseService>>(provider => dbName => new DatabaseService(mongoContext, dbName))
                .AddSingleton<IRepository<Raid>>(new RaidRepository(mongoContext, RaidCollection))
                .AddSingleton<IRepository<Installer>>(new InstallerRepository(mongoContext, RaidCollection));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // ReSharper disable once UnusedMember.Global
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            // app.UseMvc();
            // app.UseDefaultFiles();
            // app.UseStaticFiles();

            app.UseRouting();
            app.UseEndpoints(builder =>
            {
                builder.MapHub<RaidHub>("/raid");
                builder.MapControllers();
            });
            RunDiscordBot(app, env, lifetime).GetAwaiter().GetResult();
            _started = DateTime.UtcNow;
        }

        private static async Task RunDiscordBot(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            var version = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            var appVersion = Version.Parse(version);
            var versions = new Dictionary<string, string>
            {
                {"0.9.53", "Added bot messages for restart and update."},
                {"0.9.54", "Added `wipe-db` command for admin, which wipes completions, rewards and requests."},
                {"0.9.55", "`I am back!` message will be shown only on server restart."},
                {"0.9.56", "Added `total` command for banker to check total amount of currency on players accounts."},
                {"0.9.57", "Added `delete-currency` command."},
                {"0.9.58", "Changed sorting by date to descending."},
                {"0.9.59", "Crowfall Parser with Raid functionality is out! Checkout here: https://discord.gg/dR59WmFPTe"},
            };

            var services = app.ApplicationServices;
            var logger = services.GetRequiredService<ILogger<Program>>();

            try
            {
                await services.GetRequiredService<CommandHandlingService>().InitializeAsync(services);

                var client = services.GetService<DiscordSocketClient>();
                var settingsService = services.GetService<ISettingsService>();
                var mongoContext = services.GetService<MongoContext>();
                await client.LoginAsync(TokenType.Bot, settingsService.Secret);
                await client.StartAsync();
                client.Connected += () =>
                {
                    logger.LogInformation("Connected");

                    if ((DateTime.UtcNow - _started).TotalSeconds < 60)
                    {
                        Task.Run(() =>
                        {
                            SendMessageToBotChannel((settings, channel) =>
                            {
                                var message = "I am back!";

                                if (settings.Version == null || appVersion > Version.Parse(settings.Version))
                                {
                                    message += " And updated!";
                                    if (versions.ContainsKey(version))
                                    {
                                        message += " Changes:\n" + versions[version];
                                    }
                                }

                                return message;
                            }, client, settingsService, mongoContext, env, version);
                        });
                    }

                    return null;
                };

                void Callback() => SendMessageToBotChannel((settings, channel) => "Restarting. I'll be back!", client, settingsService, mongoContext, env, version);

                lifetime.ApplicationStopping.Register(Callback);

                async void Backup() => await ScheduleHelper.DumbAndSend(settingsService.DbPassword, services.GetRequiredService<IMailService>());
                if (env.IsProduction())
                {
                    var registry = new Registry();
                    registry.Schedule(Backup).ToRunEvery(0).Days().At(0, 0);
                    registry.Schedule(Backup).ToRunEvery(0).Days().At(6, 0);
                    registry.Schedule(Backup).ToRunEvery(0).Days().At(12, 0);
                    registry.Schedule(Backup).ToRunEvery(0).Days().At(18, 0);
                    JobManager.JobException += info => logger.LogError(info.Exception, "An error just happened with a scheduled job.");
                    JobManager.Initialize(registry);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "An error occurred.");
            }
        }

        private static void SendMessageToBotChannel(Func<Setting, SocketChannel, string> messageBuilder, DiscordSocketClient client, ISettingsService settingsService, MongoContext mongoContext,
            IHostingEnvironment env, string version)
        {
            foreach (var guild in client.Guilds.Where(x => env.IsProduction() || x.Id == settingsService.AmatalkId))
            {
                var settingsRepository = new SettingRepository(mongoContext, guild.Id.ToString());
                var settings = settingsRepository.FirstOrDefault();
                if (settings != null)
                {
                    var channel = guild.Channels.OfType<SocketTextChannel>().FirstOrDefault(x => x.Id == settings.ChannelId);
                    if (channel != null)
                    {
                        var message = messageBuilder(settings, channel);
                        channel.SendMessageAsync(message);
                    }

                    if (settings.Version != version)
                    {
                        settings.Version = version;
                        settingsRepository.Update(settings);
                    }
                }
            }
        }
    }
}