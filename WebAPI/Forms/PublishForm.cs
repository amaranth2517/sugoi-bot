using Microsoft.AspNetCore.Http;

namespace WebAPI.Forms
{
    public class PublishForm
    {
        public IFormFile File { get; set; }
    }
}