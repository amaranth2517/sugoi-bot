using Microsoft.AspNetCore.Http;

namespace WebAPI.Forms
{
    public class BugReportForm
    {
        public string Character { get; set; }
        public string DiscordHandle { get; set; }
        public string Comment { get; set; }
        public string SystemInfo { get; set; }
        public string Version { get; set; }
        public IFormFile Log1 { get; set; }
        public IFormFile Log2 { get; set; }
        public IFormFile Log3 { get; set; }
        public IFormFile Log4 { get; set; }
        public IFormFile Log5 { get; set; }
    }
}