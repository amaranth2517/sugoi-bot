using System.Collections.Generic;

namespace WebAPI.Hubs
{
    public class RaidInfo
    {
        public List<string> Members { get; set; }
        public string Leader { get; set; }
        public int IdleDuration { get; set; }

        public RaidInfo(List<string> members, string leader, int idleDuration)
        {
            Members = members;
            Leader = leader;
            IdleDuration = idleDuration;
        }

        public override string ToString()
        {
            return $"Leader: {Leader} ({IdleDuration}) Members: {string.Join(", ", Members)}";
        }
    }
}