﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebAPI.Models;

namespace WebAPI.Hubs
{
    public class Raid : IEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        public string Handle { get; set; }
        [BsonIgnore] public List<Computer> Computers { get; set; }
        [BsonIgnore] public int IdleDuration { get; set; }

        [BsonIgnore] public Computer Server => Computers.FirstOrDefault(x => x.IsServer);
        public List<Fight> Fights { get; set; }

        public Computer GetComputerByConnectionId(string contextConnectionId)
        {
            return Computers.FirstOrDefault(x => x.ConnectionId == contextConnectionId);
        }

        public Computer GetComputerByEndpoint(IPEndPoint endPoint)
        {
            return Computers.FirstOrDefault(x => Equals(x.EndPoint, endPoint));
        }

        public List<Computer> GetMembersComputers()
        {
            return Computers.Where(x => !x.IsServer).ToList();
        }

        public List<string> GetAllConnectionIds()
        {
            return Computers.Select(x => x.ConnectionId).ToList();
        }

        public override string ToString()
        {
            return $"{Handle}[RAID]";
        }
    }
}