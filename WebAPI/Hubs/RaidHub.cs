using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using WebAPI.Database;
using WebAPI.Models;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedMember.Global

namespace WebAPI.Hubs
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class RaidHub : Hub
    {
        private static readonly List<Raid> Raids = new List<Raid>();
        private readonly IRepository<Raid> _raidsRepository;
        private readonly ILogger<RaidHub> _logger;
        private readonly SemaphoreSlim _joinSemaphoreSlim = new SemaphoreSlim(1, 1);
        private const string ServerInfo = "ServerInfo";
        private const string Members = "Members";
        private const string Connected = "Connected";
        private const string Joining = "Joining";
        private const string Rejoin = "Rejoin";
        private const string Leaving = "Leaving";
        private const string Relayed = "Relay";
        private const string TakeOver = "TakeOver";
        private const string FightsUpdated = "FightsUpdated";

        public RaidHub(IRepository<Raid> raids, ILogger<RaidHub> logger)
        {
            _raidsRepository = raids;
            _logger = logger;
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Caller.SendAsync(Connected, DateTime.Now.ToString(CultureInfo.InvariantCulture));
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (exception == null)
                _logger.LogWarning($"{Context.ConnectionId} LOST CONNECTION");
            else
            {
                _logger.LogError(exception, $"{Context.ConnectionId} LOST CONNECTION");
                // note: give client time to reconnect before dropping connection completely
                await Task.Delay(12000);
            }

            var connectionId = Context.ConnectionId;
            var raid = GetRaidByConnectionId(connectionId);
            var computer = raid?.GetComputerByConnectionId(connectionId);
            if (computer == null)
                return;

            if (computer.ConnectionId == connectionId)
                await Leave();

            await base.OnDisconnectedAsync(exception);
        }

        private static long _id;

        public async Task Join(string handle, IPEndPoint endPoint, string characterName)
        {
            UpId();

            Computer computer;
            Raid raid;

            _logger.LogInformation($"Task {_id} waits for semaphore");

            await _joinSemaphoreSlim.WaitAsync();
            try
            {
                _logger.LogInformation($"Task {_id} enters semaphore");
                raid = Raids.FirstOrDefault(x => x.Handle == handle);

                // todo: optimise data structure
                if (raid == null)
                {
                    _logger.LogInformation($"{handle} IS A NEW RAID");
                    computer = new Computer {EndPoint = endPoint, ConnectionId = Context.ConnectionId, CharacterName = characterName, IsServer = true};
                    raid = new Raid {Handle = handle, Computers = new List<Computer> {computer}};
                    Raids.Add(raid);
                }
                else
                {
                    _logger.LogInformation($"{handle} IS EXISTING RAID");
                    computer = raid.GetComputerByEndpoint(endPoint);
                    if (computer == null)
                    {
                        _logger.LogInformation($"{endPoint} IS A NEW COMPUTER");
                        computer = new Computer {EndPoint = endPoint, ConnectionId = Context.ConnectionId, CharacterName = characterName, IsServer = raid.Server == null};
                        raid.Computers.Add(computer);
                    }
                    else
                    {
                        _logger.LogInformation($"{endPoint} IS A EXISTING COMPUTER");
                        computer.ConnectionId = Context.ConnectionId;
                        computer.CharacterName = characterName;

                        if (computer.IsServer)
                        {
                            await SendNewServerInfo(raid, computer);
                            _logger.LogInformation($"RECONNECTING {computer} IN {raid}");
                            await UpdateRaidMembersInfo(raid);
                            return;
                        }
                    }
                }
            }
            finally
            {
                _logger.LogInformation($"Task {_id} releases semaphore");
                _joinSemaphoreSlim.Release();
            }


            await Clients.Caller.SendAsync(ServerInfo, raid.Server?.EndPoint);

            if (computer.IsServer)
            {
                _logger.LogInformation($"{computer} STARTING SERVER FOR {raid}");
            }
            else if (raid.Server != null)
            {
                _logger.LogInformation($"{computer} JOINING {raid}");
                await Clients.Client(raid.Server.ConnectionId).SendAsync(Joining, computer);
                _logger.LogInformation($"ASKING {raid.Server} TO CONNECT TO {computer}");
            }

            await UpdateRaidMembersInfo(raid);
        }

        private static void UpId()
        {
            if (_id == long.MaxValue)
                _id = 0;

            _id++;
        }

        public async Task Reconnect(string handle, IPEndPoint endPoint, string characterName)
        {
            var raid = GetRaidByEndpoint(endPoint);
            var computer = raid?.GetComputerByEndpoint(endPoint);
            if (computer == null)
            {
                await Join(handle, endPoint, characterName);
                if (raid != null)
                {
                    _logger.LogInformation($"{endPoint}{characterName} REJOINED RAID {raid}");
                    await Clients.Caller.SendAsync(Rejoin, raid.Server.EndPoint);
                }
            }
            else
            {
                computer.ConnectionId = Context.ConnectionId;
                await Clients.Client(computer.ConnectionId).SendAsync(ServerInfo, raid.Server.EndPoint);
                _logger.LogInformation($"{computer} RECONNECTED TO RAID {raid}");
            }
        }

        public async Task Leave()
        {
            var raid = GetRaidByConnectionId(Context.ConnectionId);
            var computer = raid?.GetComputerByConnectionId(Context.ConnectionId);

            if (computer == null)
                return;

            _logger.LogInformation($"{computer} LEAVING {raid}");

            if (computer.IsServer)
            {
                _logger.LogInformation($"{computer} WAS SERVER IN {raid}");
                await ElectNewServer(computer, raid);
            }
            else
            {
                var server = raid.Computers.FirstOrDefault(x => x.IsServer);
                if (server?.ConnectionId != null)
                    await Clients.Client(server.ConnectionId).SendAsync(Leaving, computer);
            }

            raid.Computers.Remove(computer);
            await UpdateRaidMembersInfo(raid);
        }

        private static Raid GetRaidByConnectionId(string connectionId)
        {
            return Raids.FirstOrDefault(x => x.Computers.Any(y => y.ConnectionId == connectionId));
        }

        private static Raid GetRaidByEndpoint(IPEndPoint endPoint)
        {
            return Raids.FirstOrDefault(x => x.Computers.Any(y => Equals(y.EndPoint, endPoint)));
        }

        public async Task Relay(byte[] data, List<IPEndPoint> endPoints)
        {
            var raid = GetRaidByConnectionId(Context.ConnectionId);
            var callerComputer = raid?.GetComputerByConnectionId(Context.ConnectionId);
            var receivers = string.Join(",", endPoints);

            if (callerComputer == null)
            {
                _logger.LogError($"FAILED TO RELAY TO {receivers}. CALLER CONNECTION NOT FOUND.");
                return;
            }

            var connectionIds = new List<string>();

            foreach (var endPoint in endPoints)
            {
                var receiverComputer = raid.GetComputerByEndpoint(endPoint);
                if (receiverComputer == null)
                    _logger.LogError($"FAILED TO RELAY TO {endPoint} FROM {callerComputer}. RECEIVER CONNECTION NOT FOUND.");
                else
                    connectionIds.Add(receiverComputer.ConnectionId);
            }

            await Clients.Clients(connectionIds).SendAsync(Relayed, data, callerComputer.EndPoint);
            _logger.LogInformation($"RELAYING {Enum.GetName(typeof(Commands), data[0]) ?? "Unknown"}[{data.Length}] TO {receivers} FROM {callerComputer.EndPoint}");
        }

        public async Task ChangeServer()
        {
            var raid = GetRaidByConnectionId(Context.ConnectionId);
            var computer = raid?.GetComputerByConnectionId(Context.ConnectionId);

            if (computer == null)
            {
                _logger.LogError($"FAILED TO CHANGE SERVER FROM {Context.ConnectionId}, CALLER CONNECTION NOT FOUND");
                return;
            }

            _logger.LogInformation($"{computer} NAT BLOCKING CONNECTIONS, CHANGING SERVER");

            if (computer.IsServer)
            {
                computer.Unreachable = true;
                await ElectNewServer(computer, raid);
                await UpdateRaidMembersInfo(raid);
            }
        }

        public async Task Idle(int idle)
        {
            var raid = GetRaidByConnectionId(Context.ConnectionId);
            var computer = raid?.GetComputerByConnectionId(Context.ConnectionId);

            if (computer == null)
            {
                _logger.LogError($"FAILED TO SET IDLE {idle} FOR {Context.ConnectionId}, CALLER CONNECTION NOT FOUND");
                return;
            }

            if (!computer.IsServer)
            {
                _logger.LogError($"DENIED SETTING IDLE {idle} FOR {Context.ConnectionId}, {computer} IS NOT A SERVER");
                return;
            }

            raid.IdleDuration = idle;
            _logger.LogInformation($"SET IDLE {idle} FOR {raid}");
            await UpdateRaidMembersInfo(raid);
        }

        private async Task ElectNewServer(Computer oldServer, Raid raid)
        {
            oldServer.IsServer = false;

            var newServer = raid.Computers.FirstOrDefault(x => !x.Unreachable && !Equals(x.EndPoint, oldServer.EndPoint))
                            ?? raid.Computers.FirstOrDefault(x => !Equals(x.EndPoint, oldServer.EndPoint));

            if (newServer == null)
                return;

            newServer.IsServer = true;
            await SendNewServerInfo(raid, newServer);
            _logger.LogInformation($"ELECTING {newServer} AS NEW SERVER IN {raid}");
        }

        private async Task SendNewServerInfo(Raid raid, Computer newServer)
        {
            var membersComputers = raid.GetMembersComputers();
            await Clients.Client(newServer.ConnectionId).SendAsync(TakeOver, membersComputers);
            await Clients.Clients(membersComputers.Select(x => x.ConnectionId).ToList()).SendAsync(ServerInfo, newServer.EndPoint);
        }

        private async Task UpdateRaidMembersInfo(Raid raid)
        {
            var characters = raid.Computers.Select(x => x.CharacterName).ToList();
            if (characters.Any())
            {
                characters.Sort();
                var raidInfo = new RaidInfo(characters, raid.Server.CharacterName, raid.IdleDuration);
                await Clients.Clients(raid.Computers.Select(x => x.ConnectionId).ToList()).SendAsync(Members, raidInfo);
                _logger.LogInformation($"{raid.Handle} UPDATED: {raidInfo}");
            }
        }

        public void UploadRaidStats(DateTime started, DateTime ended, string name, Character[] characters)
        {
            var raid = GetRaidByConnectionId(Context.ConnectionId);
            var computer = raid?.GetComputerByConnectionId(Context.ConnectionId);

            if (computer != null && computer.IsServer)
            {
                raid.Fights ??= new List<Fight>();
                if (!raid.Fights.Any())
                {
                    AddFight(started, ended, name, characters, raid);
                }
                else
                {
                    var fight = raid.Fights.FirstOrDefault(x => (int) (x.Started - started.ToUniversalTime()).TotalSeconds == 0);
                    if (fight == null)
                    {
                        AddFight(started, ended, name, characters, raid);
                    }
                    else
                    {
                        fight.Ended = ended;
                        fight.Name = name;
                        fight.Characters = characters;
                    }
                }

                var entity = _raidsRepository.Get(x => x.Handle == raid.Handle);
                if (entity == null)
                    _raidsRepository.Add(raid);
                else
                {
                    raid.Id = entity.Id;
                    _raidsRepository.Update(raid);
                }
            }
            else
            {
                _logger.LogWarning($"{computer} IS NOT SERVER IN {raid} AND TRIES TO UPLOAD RAID STATS");
            }
        }

        private void AddFight(DateTime started, DateTime ended, string name, Character[] characters, Raid raid)
        {
            raid.Fights.Add(new Fight {Started = started, Ended = ended, Characters = characters, Name = name});
            Clients.Clients(raid.GetAllConnectionIds()).SendAsync(FightsUpdated, raid.Fights.Count);
        }

        public int GetRaidsCount()
        {
            return Raids.Count;
        }
    }
}