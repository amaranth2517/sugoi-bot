using System;
using WebAPI.Models;

namespace WebAPI.Hubs
{
    public class Fight
    {
        public string Name { get; set; }
        public DateTime Started { get; set; }
        public DateTime Ended { get; set; }
        public Character[] Characters { get; set; }
    }
}