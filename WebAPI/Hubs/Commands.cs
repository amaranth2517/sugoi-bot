namespace WebAPI.Hubs
{
    public enum Commands : byte
    {
        Join = 0,
        Leave,
        ClientStats,
        Raid,
        RaidStats,
        IncompatibleProtocol,
        KeepAlive,
        Punch,
        Reconnect
    }
}