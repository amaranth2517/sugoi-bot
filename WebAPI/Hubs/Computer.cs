﻿using System.Net;

namespace WebAPI.Hubs
{
    public class Computer
    {
        public IPEndPoint EndPoint { get; set; }

        public bool Unreachable { get; set; }

        public bool IsServer { get; set; }
        public string ConnectionId { get; set; }
        public string CharacterName { get; set; }

        // todo: some kind of latency

        public override string ToString()
        {
            var type = IsServer ? "SERVER" : "CLIENT";
            
            return $"{EndPoint}[{type} - {CharacterName}]";
        }
    }
}