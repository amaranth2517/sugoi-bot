﻿using System;
using System.ComponentModel;
using System.Linq;
using WebAPI.Attributes;

namespace WebAPI.Extensions
{
    public static class EnumExtensions
    {
        public static string Print<T>(this T enumerationValue) where T : struct
        {
            var type = enumerationValue.GetType();

            if (!type.IsEnum)
                throw new ArgumentException("EnumerationValue must be of Enum type");

            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo == null || memberInfo.Length <= 0)
                return enumerationValue.ToString();

            var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attrs != null && attrs.Length > 0
                ? ((DescriptionAttribute) attrs[0]).Description
                : enumerationValue.ToString();
        }

        public static string Sign<T>(this T enumerationValue) where T : struct
        {
            var type = enumerationValue.GetType();

            if (!type.IsEnum)
                throw new ArgumentException("EnumerationValue must be of Enum type");

            var memberInfo = type.GetMember(enumerationValue.ToString());
            if (memberInfo == null || memberInfo.Length <= 0)
                return enumerationValue.ToString();

            var attrs = memberInfo[0].GetCustomAttributes(typeof(SlugAttribute), false);

            return attrs != null && attrs.Length > 0
                ? ((SlugAttribute) attrs[0]).Sign
                : enumerationValue.ToString();
        }
    }

    public class Enum<T> where T : struct, IConvertible
    {
        public static bool TryParse(string sign, out T result)
        {
            if (string.IsNullOrEmpty(sign))
            {
                result = new T();
                return false;
            }

            var field = typeof(T).GetFields()
                .FirstOrDefault(x => x.GetCustomAttributes(typeof(SlugAttribute), false).OfType<SlugAttribute>().FirstOrDefault()?.Sign == sign);

            if (field == null)
            {
                result = new T();
                return false;
            }

            result = (T) field.GetValue(null);
            return true;
        }
    }
}