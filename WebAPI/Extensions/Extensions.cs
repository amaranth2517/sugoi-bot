using System.Collections.Generic;
using Discord;
using Discord.WebSocket;
using MongoDB.Bson;
using WebAPI.Database;
using WebAPI.Models;

namespace WebAPI.Extensions
{
    public static class Extensions
    {
        public static string GetUsername(this IUser user)
        {
            return user is SocketGuildUser guildUser && !string.IsNullOrEmpty(guildUser.Nickname) ? guildUser.Nickname : user.Username;
        }

        public static string PrintCurrency(this Quest quest, IRepository<Currency> currencies)
        {
            return quest.Version == 2 ? currencies.Get(quest.CurrencyId).Description : quest.RewardType.Print();
        }

        public static T GetBySlug<T>(this IRepository<T> repository, string slugOrId) where T : IEntity, IHaveSlug
        {
            ObjectId.TryParse(slugOrId, out var id);
            return repository.Get(x => x.Slug == slugOrId || x.Id == id);
        }

        public static IEnumerable<T> GetByPlayer<T>(this IRepository<T> repository, ulong playerId) where T : IHavePlayer
        {
            return repository.GetAll(x => x.PlayerId == playerId);
        }

        private static void ConvertRewards(this Reward record, IRepository<Currency> currencies)
        {
            record.Points = new Dictionary<string, float>
            {
                {currencies.GetBySlug(RewardType.Coins.Sign()).Id.ToString(), record.Coins},
                {currencies.GetBySlug(RewardType.GearPoints.Sign()).Id.ToString(), record.GearPoints},
                {currencies.GetBySlug(RewardType.JewelryPoints.Sign()).Id.ToString(), record.JewelryPoints},
                {currencies.GetBySlug(RewardType.VesselPoints.Sign()).Id.ToString(), record.VesselPoints}
            };

            record.Version = 2;
        }

        public static bool HasRewards(this Reward record, Currency currency, IRepository<Currency> currencies)
        {
            if (record.Version != 2)
                record.ConvertRewards(currencies);

            return record.Points != null && record.Points.ContainsKey(currency.Id.ToString());
        }

        public static void UpdateRewards(this Reward record, Currency currency, float amount, IRepository<Currency> currencies)
        {
            if (record.Version != 2)
                record.ConvertRewards(currencies);

            if (record.Points == null)
                record.Points = new Dictionary<string, float>();

            if (record.Points.ContainsKey(currency.Id.ToString()))
                record.Points[currency.Id.ToString()] += amount;
            else
                record.Points.Add(currency.Id.ToString(), amount);
        }
    }
}