using System.Text;

namespace WebAPI.Extensions
{
    public class ResponseBuilder
    {
        private readonly int _limit;
        private readonly StringBuilder _builder = new StringBuilder();
        private int _overflows;

        public ResponseBuilder(int limit = 2000)
        {
            _limit = limit - 35;
        }

        public void AppendLine(string line = "")
        {
            if (_builder.Length + line.Length > _limit)
            {
                _overflows++;
            }
            else
            {
                _builder.AppendLine(line);
            }
        }

        public override string ToString()
        {
            if (_overflows > 0)
                _builder.Append($"and another {_overflows} record(-s)...");

            return _builder.ToString();
        }
    }
}