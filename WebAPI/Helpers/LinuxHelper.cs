﻿using System.Diagnostics;
using NLog;

namespace WebAPI.Helpers
{
    public static class LinuxHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public static void Bash(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            process.Start();

            var output = process.StandardOutput.ReadToEnd();
            if (!string.IsNullOrEmpty(output))
                Logger.Info("Process finished: " + output);

            var error = process.StandardError.ReadToEnd();
            if (!string.IsNullOrEmpty(error))
                Logger.Error("Process failed: " + error);

            process.WaitForExit();
        }
    }
}