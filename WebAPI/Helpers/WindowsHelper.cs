﻿using System.Diagnostics;
using NLog;

namespace WebAPI.Helpers
{
    public static class WindowsHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static void Cmd(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"") + " && exit";

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd",
                    Arguments = $"/K \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            process.Start();
            Logger.Info(process.StandardOutput.ReadToEnd());
            process.WaitForExit();
        }
    }
}