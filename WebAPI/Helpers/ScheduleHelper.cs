﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Helpers
{
    public static class ScheduleHelper
    {
        public static async Task DumbAndSend(string password, IMailService mailService)
        {
            const string dir = "/app/";

            ("mongodump --host SugoiBot-shard-0/sugoibot-shard-00-00-j9y9n.mongodb.net:27017,sugoibot-shard-00-01-j9y9n.mongodb.net:27017,sugoibot-shard-00-02-j9y9n.mongodb.net:27017 " +
             $"--ssl --username sugoi_bot --password {password} --authenticationDatabase admin").Bash();

//            "7z a sugoi_bot dump".Cmd();
            $"tar -zcvf {dir}sugoi_bot.tar.gz dump/".Bash();

//            const string fileName = "sugoi_bot.7z";
            var fileName = $"{dir}sugoi_bot.tar.gz";

            await mailService.SendAsync("Sugoi DB backup", string.Empty, new List<Attachment>
            {
                new Attachment
                {
                    Data = Convert.ToBase64String(await File.ReadAllBytesAsync(fileName)),
                    Name = fileName,
//                            Type = "application/x-7z-compressed"
                    Type = "application/tar+gzip"
                }
            });
        }
    }
}