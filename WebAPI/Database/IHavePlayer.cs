﻿namespace WebAPI.Database
{
    public interface IHavePlayer
    {
        ulong PlayerId { get; }
    }
}