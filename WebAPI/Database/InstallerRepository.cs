using WebAPI.Models;

namespace WebAPI.Database
{
    public class InstallerRepository : BaseRepository<Installer>
    {
        public InstallerRepository(MongoContext context, string dbName) : base(context, dbName, "Installer")
        {
        }
    }
}