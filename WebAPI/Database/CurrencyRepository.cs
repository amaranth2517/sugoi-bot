﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class CurrencyRepository : BaseRepository<Currency>
    {
        public CurrencyRepository(MongoContext context, string dbName) : base(context, dbName, "Currency")
        {
        }
    }
}