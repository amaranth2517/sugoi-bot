﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class QuestRepository : BaseRepository<Quest>
    {
        public QuestRepository(MongoContext context, string dbName) : base(context, dbName, "Quest")
        {
        }
    }
}