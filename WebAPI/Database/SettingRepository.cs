﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class SettingRepository : BaseRepository<Setting>
    {
        public SettingRepository(MongoContext context, string dbName) : base(context, dbName, "Setting")
        {
        }
    }
}