﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class CompletionRepository : BaseRepository<Completion>
    {
        public CompletionRepository(MongoContext context, string dbName) : base(context, dbName, "Completion")
        {
        }
    }
}