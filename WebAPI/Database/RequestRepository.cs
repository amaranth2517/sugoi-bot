﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class RequestRepository : BaseRepository<Request>
    {
        public RequestRepository(MongoContext context, string dbName) : base(context, dbName, "Request")
        {
        }
    }
}