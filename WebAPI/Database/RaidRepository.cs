﻿using WebAPI.Hubs;

namespace WebAPI.Database
{
    public class RaidRepository : BaseRepository<Raid>
    {
        public RaidRepository(MongoContext context, string dbName) : base(context, dbName, "Raid")
        {
        }
    }
}