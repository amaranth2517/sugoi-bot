﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Bson;

namespace WebAPI.Database
{
    public interface IRepository<T>
    {
        bool Add(T entity);
        bool Update(T entity);
        bool Delete(ObjectId id);
        T Get(ObjectId id);
        T Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate);
        T FirstOrDefault();
    }
}