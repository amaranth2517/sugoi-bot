﻿using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using WebAPI.Services;

namespace WebAPI.Database
{
    public class MongoContext
    {
        public MongoClient Client { get; }

        public MongoContext(ISettingsService settingsService)
        {
            var pack = new ConventionPack {new IgnoreExtraElementsConvention(true)};
            ConventionRegistry.Register("Ignore Extra Field Convention", pack, t => true);
            
            Client = new MongoClient(settingsService.ConnectionString);
        }
    }
}