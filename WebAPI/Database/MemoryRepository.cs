using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Bson;
using WebAPI.Models;

namespace WebAPI.Database
{
    public class MemoryRepository<T> : IRepository<T> where T : IEntity
    {
        private readonly List<T> _entities = new List<T>();

        public bool Add(T entity)
        {
            _entities.Add(entity);
            return true;
        }

        public bool Update(T entity)
        {
            if (_entities.Any(x => x.Id == entity.Id))
                Delete(entity.Id);

            Add(entity);
            return true;
        }

        public bool Delete(ObjectId id)
        {
            _entities.RemoveAll(x => x.Id == id);
            return true;
        }

        public T Get(ObjectId id)
        {
            return _entities.FirstOrDefault(x => x.Id == id);
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _entities.FirstOrDefault(x => predicate.Compile()(x));
        }

        public IEnumerable<T> GetAll()
        {
            return _entities;
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _entities.Where(x => predicate.Compile()(x));
        }

        public T FirstOrDefault()
        {
            return _entities.FirstOrDefault();
        }

        public T GetBySlug(string slug)
        {
            return (T) _entities.OfType<IHaveSlug>().FirstOrDefault(x => x.Slug == slug);
        }

        public IEnumerable<T> GetByPlayer(ulong playerId)
        {
            return _entities.OfType<IHavePlayer>().Where(x => x.PlayerId == playerId).OfType<T>();
        }
    }
}