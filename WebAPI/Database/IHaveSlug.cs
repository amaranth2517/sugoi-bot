﻿namespace WebAPI.Database
{
    public interface IHaveSlug
    {
        string Slug { get; }
    }
}