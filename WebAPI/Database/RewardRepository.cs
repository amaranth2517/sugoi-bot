﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class RewardRepository : BaseRepository<Reward>
    {
        public RewardRepository(MongoContext context, string dbName) : base(context, dbName, "Reward")
        {
        }
    }
}