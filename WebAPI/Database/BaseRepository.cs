﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;
using WebAPI.Models;

namespace WebAPI.Database
{
    public abstract class BaseRepository<T> : IRepository<T> where T : IEntity
    {
        private readonly IMongoCollection<T> _collection;
        private const int SchemaVersion = 2;

        protected BaseRepository(MongoContext context, string dbName, string tableName)
        {
            _collection = context.Client.GetDatabase(dbName).GetCollection<T>(tableName);
        }

        public bool Add(T entity)
        {
            if (entity is IVersion versionedEntity)
                versionedEntity.Version = SchemaVersion;

            if (entity is ITrackedEntity trackedEntity)
            {
                trackedEntity.CreatedAt = DateTime.Now;
                trackedEntity.UpdatedAt = DateTime.Now;
            }
            
            _collection.InsertOne(entity);
            return true;
        }

        public bool Update(T entity)
        {
            var filter = Builders<T>.Filter.Eq(nameof(entity.Id), entity.Id);
            var update = GetUpdateDefinition(entity);

            if (entity is ITrackedEntity trackedEntity)
                trackedEntity.UpdatedAt = DateTime.Now;
            
            return _collection.UpdateOne(filter, update).IsAcknowledged;
        }

        private static UpdateDefinition<T> GetUpdateDefinition(T entity)
        {
            var type = entity.GetType();
            var propertyInfos = type.GetProperties().Where(x => x.Name != nameof(entity.Id));

            var updateDefinitions = new List<UpdateDefinition<T>>();
            foreach (var prop in propertyInfos)
            {
                var value = prop.GetValue(entity);
                var updateDefinition = value == null ? Builders<T>.Update.Unset(prop.Name) : Builders<T>.Update.Set(prop.Name, value);
                updateDefinitions.Add(updateDefinition);
            }

            return Builders<T>.Update.Combine(updateDefinitions);
        }

        public bool Delete(ObjectId id)
        {
            var filter = Builders<T>.Filter.Eq("Id", id);
            return _collection.DeleteOne(filter).IsAcknowledged;
        }
        
        public T Get(ObjectId id)
        {
            return _collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _collection.Find(predicate).FirstOrDefault();
        }

        public T FirstOrDefault()
        {
            return _collection.Find(x => true).FirstOrDefault();
        }

        // todo: refactor to async for performance
        public IEnumerable<T> GetAll()
        {
            return _collection.Find(x => true).ToEnumerable();
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _collection.Find(predicate).ToEnumerable();
        }
    }
}