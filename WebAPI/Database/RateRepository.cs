﻿using WebAPI.Models;

namespace WebAPI.Database
{
    public class RateRepository : BaseRepository<Rate>
    {
        public RateRepository(MongoContext context, string dbName) : base(context, dbName, "Rate")
        {
        }
    }
}