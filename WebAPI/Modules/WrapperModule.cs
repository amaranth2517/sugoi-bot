﻿using System.Threading.Tasks;
using Discord.Commands;
using WebAPI.Models;

namespace WebAPI.Modules
{
    public class WrapperModule : ModuleBase<SocketCommandContext>
    {
        private IContext _context;

        protected IContext ModuleContext
            => _context ?? new ModuleContext(Context.User, Context.Message.MentionedRoles, Context.Message.MentionedUsers, Context.Message.Attachments, Context.Message.MentionedChannels);

        /*
         * Used to inject Mock context for unit tests, because SDK does not allow mocking it. TODO: report on github.
         */
        public void SetContext(IContext context)
        {
            _context = context;
        }

        protected Task Respond(Response response, Languages language = Languages.English)
        {
            return ReplyAsync(language == Languages.Russian && !string.IsNullOrEmpty(response.MessageRu) ? response.MessageRu : response.Message);
        }
    }
}