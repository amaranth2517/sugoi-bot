﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using WebAPI.Database;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Modules
{
    public class QuestsModule : WrapperModule
    {
        private readonly Func<SocketGuild, IGuildService> _guildServiceFactory;
        private readonly Func<string, IRepository<Quest>> _questsFactory;
        private readonly Func<string, IRepository<Completion>> _completionsFactory;
        private readonly Func<string, IRepository<Reward>> _rewardsFactory;
        private readonly Func<string, IRepository<Rate>> _ratesFactory;
        private readonly Func<string, IRepository<Setting>> _settingsFactory;
        private readonly Func<string, IRepository<Request>> _requestsFactory;
        private readonly Func<string, IRepository<Currency>> _currenciesFactory;

        private QuestingService _service;

        public QuestsModule(Func<SocketGuild, IGuildService> guildServiceFactory,
            Func<string, IRepository<Quest>> questsFactory,
            Func<string, IRepository<Completion>> completionsFactory,
            Func<string, IRepository<Reward>> rewardsFactory,
            Func<string, IRepository<Rate>> ratesFactory,
            Func<string, IRepository<Setting>> settingsFactory,
            Func<string, IRepository<Request>> requestsFactory,
            Func<string, IRepository<Currency>> currenciesFactory)
        {
            _guildServiceFactory = guildServiceFactory;
            _questsFactory = questsFactory;
            _completionsFactory = completionsFactory;
            _rewardsFactory = rewardsFactory;
            _ratesFactory = ratesFactory;
            _settingsFactory = settingsFactory;
            _requestsFactory = requestsFactory;
            _currenciesFactory = currenciesFactory;
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            var guildService = _guildServiceFactory(Context.Guild);
            var dbName = guildService.GetDbName();
            _service = new QuestingService(guildService, _questsFactory(dbName), _completionsFactory(dbName), _rewardsFactory(dbName), _ratesFactory(dbName), _settingsFactory(dbName),
                _currenciesFactory(dbName), _requestsFactory(dbName));
        }


        // todo: we can assign roles in discord: harvester, crafter, pvper and people will get notification
        [Command("quests")]
        public Task Quests(string type = null)
        {
            return Respond(_service.Quests(type));
        }

        [Command("квесты")]
        public Task QuestsRu(string type = null)
        {
            return Respond(_service.Quests(type), Languages.Russian);
        }

        [Command("add-quest")]
        public Task AddQuest(params string[] args)
        {
            return Respond(_service.AddQuest(ModuleContext, args));
        }

        [Command("добавить-квест")]
        public Task AddQuestRu(params string[] args)
        {
            return Respond(_service.AddQuest(ModuleContext, args), Languages.Russian);
        }

        [Command("update-quest")]
        public Task UpdateQuest(params string[] args)
        {
            return Respond(_service.UpdateQuest(ModuleContext, args));
        }

        [Command("редактировать-квест")]
        public Task UpdateQuestRu(params string[] args)
        {
            return Respond(_service.UpdateQuest(ModuleContext, args), Languages.Russian);
        }

        [Command("disable-quest")]
        public Task DisableQuest(string idOrSlug)
        {
            return Respond(_service.DisableQuest(ModuleContext, idOrSlug));
        }

        [Command("выключить-квест")]
        public Task DisableQuestRu(string idOrSlug)
        {
            return Respond(_service.DisableQuest(ModuleContext, idOrSlug), Languages.Russian);
        }

        [Command("enable-quest")]
        public Task EnableQuest(string idOrSlug)
        {
            return Respond(_service.EnableQuest(ModuleContext, idOrSlug));
        }

        [Command("включить-квест")]
        public Task EnableQuestRu(string idOrSlug)
        {
            return Respond(_service.EnableQuest(ModuleContext, idOrSlug), Languages.Russian);
        }

        [Command("add-title")]
        public Task AddTitle(string questIdOrSlug, int completions, params string[] name)
        {
            return Respond(_service.AddTitle(ModuleContext, questIdOrSlug, completions, name));
        }

        [Command("добавить-титул")]
        public Task AddTitleRu(string questIdOrSlug, int completions, params string[] name)
        {
            return Respond(_service.AddTitle(ModuleContext, questIdOrSlug, completions, name), Languages.Russian);
        }

        [Command("turn")]
        public Task Turn(string questIdOrSlug, string amount = null, string rewardSlug = null, params string[] args)
        {
            return Respond(_service.Turn(ModuleContext, questIdOrSlug, amount, rewardSlug, args));
        }

        [Command("сдать")]
        public Task TurnRu(string questIdOrSlug, string amount = null, string rewardSlug = null, params string[] args)
        {
            return Respond(_service.Turn(ModuleContext, questIdOrSlug, amount, rewardSlug, args), Languages.Russian);
        }

        [Command("rewards")]
        public Task Rewards(string player = null)
        {
            return Respond(_service.Rewards(ModuleContext, player));
        }

        [Command("награды")]
        public Task RewardsRu(string player = null)
        {
            return Respond(_service.Rewards(ModuleContext, player), Languages.Russian);
        }

        [Command("completions")]
        public Task Completions(string playerOrQuest = null)
        {
            return Respond(_service.Completions(ModuleContext, playerOrQuest));
        }

        [Command("сдачи")]
        public Task CompletionsRu(string playerOrQuest = null)
        {
            return Respond(_service.Completions(ModuleContext, playerOrQuest), Languages.Russian);
        }

        [Command("titles")]
        public Task Titles(string player = null)
        {
            return Respond(_service.Titles(ModuleContext, player));
        }

        [Command("титулы")]
        public Task TitlesRu(string player = null)
        {
            return Respond(_service.Titles(ModuleContext, player), Languages.Russian);
        }

        [Command("rates")]
        public Task Rates()
        {
            return Respond(_service.Rates(ModuleContext));
        }

        [Command("обмен")]
        public Task RatesRu()
        {
            return Respond(_service.Rates(ModuleContext), Languages.Russian);
        }

        [Command("rate")]
        public Task Rate(float amount1, string currency1, float amount2, string currency2)
        {
            return Respond(_service.Rate(ModuleContext, amount1, currency1, amount2, currency2));
        }

        [Command("курс")]
        public Task RateRu(float amount1, string currency1, float amount2, string currency2)
        {
            return Respond(_service.Rate(ModuleContext, amount1, currency1, amount2, currency2), Languages.Russian);
        }

        [Command("convert")]
        public Task Convert(string amount, string currency1, string currency2)
        {
            return Respond(_service.Convert(ModuleContext, amount, currency1, currency2));
        }

        [Command("обменять")]
        public Task ConvertRu(string amount, string currency1, string currency2)
        {
            return Respond(_service.Convert(ModuleContext, amount, currency1, currency2), Languages.Russian);
        }

        [Command("total")]
        public Task Total()
        {
            return Respond(_service.Total(ModuleContext));
        }

        [Command("всего")]
        public Task TotalRu()
        {
            return Respond(_service.Total(ModuleContext));
        }
    }
}