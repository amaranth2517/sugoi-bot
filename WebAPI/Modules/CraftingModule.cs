﻿using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using WebAPI.Database;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Modules
{
    public class CraftingModule : WrapperModule
    {
        private readonly Func<SocketGuild, IGuildService> _guildServiceFactory;
        private readonly Func<string, IRepository<Reward>> _rewardsFactory;
        private readonly Func<string, IRepository<Request>> _requestsFactory;
        private readonly Func<string, IRepository<Setting>> _settingsFactory;
        private readonly Func<string, IRepository<Currency>> _currenciesFactory;

        private CraftingService _service;

        public CraftingModule(
            Func<SocketGuild, IGuildService> guildServiceFactory,
            Func<string, IRepository<Reward>> rewardsFactory,
            Func<string, IRepository<Request>> requestsFactory,
            Func<string, IRepository<Setting>> settingsFactory,
            Func<string, IRepository<Currency>> currenciesFactory)
        {
            _guildServiceFactory = guildServiceFactory;
            _rewardsFactory = rewardsFactory;
            _requestsFactory = requestsFactory;
            _settingsFactory = settingsFactory;
            _currenciesFactory = currenciesFactory;
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            var guildService = _guildServiceFactory(Context.Guild);
            var dbName = guildService.GetDbName();
            _service = new CraftingService(guildService, _rewardsFactory(dbName), _requestsFactory(dbName), _settingsFactory(dbName), _currenciesFactory(dbName));
        }


        [Command("request")]
        public Task Request(string professionRaw, params string[] args)
        {
            return Respond(_service.Request(ModuleContext, professionRaw, args));
        }

        [Command("заказ")]
        public Task RequestRu(string professionRaw, params string[] args)
        {
            return Respond(_service.Request(ModuleContext, professionRaw, args), Languages.Russian);
        }

        [Command("requests")]
        public Task RequestsCommand(params string[] args)
        {
            return Respond(_service.Requests(ModuleContext));
        }

        [Command("заказы")]
        public Task RequestsCommandRu(params string[] args)
        {
            return Respond(_service.Requests(ModuleContext), Languages.Russian);
        }

        [Command("cancel")]
        public Task Cancel(string id)
        {
            return Respond(_service.Cancel(ModuleContext, id));
        }

        [Command("отменить")]
        public Task CancelRu(string id)
        {
            return Respond(_service.Cancel(ModuleContext, id), Languages.Russian);
        }

        [Command("crafting")]
        public Task Crafting(string id)
        {
            return Respond(_service.Crafting(ModuleContext, id));
        }

        [Command("скрафчу")]
        public Task CraftingRu(string id)
        {
            return Respond(_service.Crafting(ModuleContext, id), Languages.Russian);
        }

        [Command("crafted")]
        public Task Crafted(string id, string dummy = null)
        {
            return Respond(_service.Crafted(ModuleContext, id, dummy));
        }

        [Command("скрафтил")]
        public Task CraftedRu(string id, string dummy = null)
        {
            return Respond(_service.Crafted(ModuleContext, id, dummy), Languages.Russian);
        }

        [Command("crafters")]
        public Task Crafters(string profession)
        {
            return Respond(_service.Crafters(ModuleContext));
        }

        [Command("крафтеры")]
        public Task CraftersRu(string profession)
        {
            return Respond(_service.Crafters(ModuleContext), Languages.Russian);
        }

        [Command("profession")]
        public Task Profession(string profession)
        {
            return Respond(_service.Profession(ModuleContext));
        }

        [Command("профессия")]
        public Task ProfessionRu(string profession)
        {
            return Respond(_service.Profession(ModuleContext), Languages.Russian);
        }
    }
}