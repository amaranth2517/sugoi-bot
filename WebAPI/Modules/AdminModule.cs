﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using WebAPI.Database;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Modules
{
    public class AdminModule : WrapperModule
    {
        private readonly Func<SocketGuild, IGuildService> _guildServiceFactory;
        private readonly Func<string, IRepository<Setting>> _settingsFactory;
        private readonly Func<string, IRepository<Currency>> _currenciesFactory;
        private readonly Func<string, IDatabaseService> _databaseServiceFactory;

        private AdminService _service;

        public AdminModule(Func<SocketGuild, IGuildService> guildServiceFactory,
            Func<string, IRepository<Setting>> settingsFactory,
            Func<string, IRepository<Currency>> currenciesFactory,
            Func<string, IDatabaseService> databaseServiceFactory)
        {
            _guildServiceFactory = guildServiceFactory;
            _settingsFactory = settingsFactory;
            _currenciesFactory = currenciesFactory;
            _databaseServiceFactory = databaseServiceFactory;
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            var guildService = _guildServiceFactory(Context.Guild);
            var dbName = guildService.GetDbName();
            _service = new AdminService(guildService, _settingsFactory(dbName), _currenciesFactory(dbName), _databaseServiceFactory(dbName));
        }

        [Command("quester")]
        public Task Quester(string roleRaw = null)
        {
            return Respond(_service.Quester(ModuleContext));
        }

        [Command("квестоплет")]
        public Task QuesterRu(string roleRaw = null)
        {
            return Respond(_service.Quester(ModuleContext), Languages.Russian);
        }

        [Command("channel")]
        public Task Channel(string channelRaw)
        {
            return Respond(_service.Channel(ModuleContext));
        }

        [Command("канал")]
        public Task ChannelRu(string channelRaw)
        {
            return Respond(_service.Channel(ModuleContext), Languages.Russian);
        }

        [Command("banker")]
        public Task Banker(string roleRaw = null)
        {
            return Respond(_service.Banker(ModuleContext));
        }

        [Command("банкир")]
        public Task BankerRu(string roleRaw = null)
        {
            return Respond(_service.Banker(ModuleContext), Languages.Russian);
        }

        [Command("prefix")]
        public Task Prefix(string prefix = null)
        {
            return Respond(_service.Prefix(ModuleContext, prefix));
        }

        [Command("префикс")]
        public Task PrefixRu(string prefix = null)
        {
            return Respond(_service.Prefix(ModuleContext, prefix), Languages.Russian);
        }

        [Command("professions")]
        public Task Professions(params string[] professions)
        {
            return Respond(_service.Professions(ModuleContext));
        }

        [Command("профессии")]
        public Task ProfessionsRu(params string[] professions)
        {
            return Respond(_service.Professions(ModuleContext), Languages.Russian);
        }

        [Command("add-currency")]
        public Task AddCurrency(string slug, params string[] args)
        {
            var description = string.Join(" ", args.Where(x => !x.StartsWith("@") && !x.StartsWith("<@")));
            return Respond(_service.AddCurrency(ModuleContext, slug, description));
        }

        [Command("добавить-валюту")]
        public Task AddCurrencyRu(string slug, params string[] args)
        {
            var description = string.Join(" ", args.Where(x => !x.StartsWith("@") && !x.StartsWith("<@")));
            return Respond(_service.AddCurrency(ModuleContext, slug, description), Languages.Russian);
        }

        [Command("update-currency")]
        public Task UpdateCurrency(string slug, string newSlug, params string[] args)
        {
            var description = string.Join(" ", args.Where(x => !x.StartsWith("@") && !x.StartsWith("<@")));
            return Respond(_service.UpdateCurrency(ModuleContext, slug, newSlug, description));
        }

        [Command("редактировать-валюту")]
        public Task UpdateCurrencyRu(string slug, string newSlug, params string[] args)
        {
            var description = string.Join(" ", args.Where(x => !x.StartsWith("@") && !x.StartsWith("<@")));
            return Respond(_service.UpdateCurrency(ModuleContext, slug, newSlug, description), Languages.Russian);
        }

        [Command("delete-currency")]
        public Task DeleteCurrency(string slug)
        {
            return Respond(_service.DeleteCurrency(ModuleContext, slug));
        }

        [Command("удалить-валюту")]
        public Task DeleteCurrencyRu(string slug, string newSlug, params string[] args)
        {
            return Respond(_service.DeleteCurrency(ModuleContext, slug), Languages.Russian);
        }

        [Command("wipe-db")]
        public Task WipeDB(string token = null)
        {
            return Respond(_service.WipeDB(ModuleContext, token, DateTime.UtcNow));
        }

        [Command("стереть-бд")]
        public Task WipeDBRu(string token = null)
        {
            return Respond(_service.WipeDB(ModuleContext, token, DateTime.UtcNow), Languages.Russian);
        }
    }
}