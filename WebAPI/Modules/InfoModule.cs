﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Modules
{
    public class InfoModule : WrapperModule
    {
        private readonly Func<SocketGuild, IGuildService> _guildServiceFactory;
        private readonly Func<string, IRepository<Setting>> _settingsFactory;
        private readonly Func<string, IRepository<Currency>> _currenciesFactory;
        private IGuildService _guildService;
        private IRepository<Setting> _settings;
        private IRepository<Currency> _currencies;

        public InfoModule(Func<SocketGuild, IGuildService> guildServiceFactory, Func<string, IRepository<Setting>> settingsFactory, Func<string, IRepository<Currency>> currenciesFactory)
        {
            _guildServiceFactory = guildServiceFactory;
            _settingsFactory = settingsFactory;
            _currenciesFactory = currenciesFactory;
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            _guildService = _guildServiceFactory(Context.Guild);
            _settings = _settingsFactory(_guildService.GetDbName());
            _currencies = _currenciesFactory(_guildService.GetDbName());
        }

        [Command("info")]
        public Task Info(string arg = null)
        {
            return SendInfo(arg);
        }

        [Command("help")]
        public Task Help(string arg = null)
        {
            return SendInfo(arg);
        }

        [Command("commands")]
        public Task Commands(string arg = null)
        {
            return SendInfo(arg);
        }

        // todo: auto-generate from attributes
        private Task<IUserMessage> SendInfo(string arg)
        {
            var response = new ResponseBuilder();
            var currencies = string.Join(", ", _currencies.GetAll().Select(x => x.Slug));
            var settings = _settings.FirstOrDefault();
            var prefix = settings?.Prefix;

            if (string.IsNullOrEmpty(prefix))
                prefix = $"{prefix}";

            response.AppendLine("@ requires mention, [] is optional parameter, # is identifier (e.g. 5b20466f7e4440132c6c3c63)");
            response.AppendLine();
            response.AppendLine($"```Currencies:  {currencies}");
            response.AppendLine($"{prefix}rewards     [@Player]");
            response.AppendLine($"{prefix}quests      [show-ids]");
            response.AppendLine($"{prefix}titles      [@Player]");
            response.AppendLine($"{prefix}completions [@Player]");
            response.AppendLine($"{prefix}crafters    @Profession");
            response.AppendLine($"{prefix}requests    [@Player] [@Profession]");
            response.AppendLine($"{prefix}rates                                                    - Currency exchange rates");
            response.AppendLine($"{prefix}convert     Amount currency1 currency2                   - Exchange currency1 for currency2");
            response.AppendLine($"{prefix}profession  @Profession                                  - Assign/remove your profession");
            response.AppendLine($"{prefix}request     @Profession [Amount] [Currency] Description  - Request craft of an item(-s)");
            response.AppendLine($"{prefix}cancel      #requestId                                   - Cancel crafting or request");
            response.AppendLine($"{prefix}crafting    #requestId                                   - Assign yourself to craft item");
            response.AppendLine($"{prefix}crafted     @Profession                                  - Tell how many you crafted");
            response.AppendLine($"{prefix}crafted     #requestId                                   - Mark request as crafted");
            response.AppendLine($"Optional: Post screenshot with {prefix}crafted command as description to save screenshot.```");

            if (settings != null)
            {
                // todo: refactor to right place
                if (_guildService.IsUserInRole(ModuleContext.MentionedOrCurrentUser, settings.Banker))
                {
                    response.AppendLine();
                    response.AppendLine($"```Turning quests in:");
                    response.AppendLine($"{prefix}rate Amount1 Currency1 Amount2 Currency2");
                    response.AppendLine($"{prefix}total");
                    response.AppendLine($"{prefix}turn #questSlug Amount-Of-Currency Currency @Player [@OtherPlayers]");
                    response.AppendLine($"Optional: Post screenshot with {prefix}turn command as description to save screenshot.```");
                }

                if (_guildService.IsUserInRole(ModuleContext.MentionedOrCurrentUser, settings.Quester))
                {
                    response.AppendLine();
                    response.AppendLine($"```Quest management (Type: harvest|craft|pvp):");
                    response.AppendLine($"{prefix}add-quest     -n Name -t Type -a Amount-Of-Currency -c Currency -d Description");
                    response.AppendLine($"{prefix}update-quest  #questSlug -n Name -t Type -a Amount -c Currency -d Description");
                    response.AppendLine($"{prefix}disable-quest #questSlug");
                    response.AppendLine($"{prefix}enable-quest  #questSlug");
                    response.AppendLine($"{prefix}add-title     #questSlug Amount Name```");
                }
            }

            if (_guildService.IsUserAdmin(ModuleContext.MentionedOrCurrentUser.Id))
            {
                response.AppendLine();
                response.AppendLine("```Bot settings:");
                response.AppendLine($"{prefix}quester         @Role");
                response.AppendLine($"{prefix}banker          @Role");
                response.AppendLine($"{prefix}prefix          string");
                response.AppendLine($"{prefix}channel         #channel");
                response.AppendLine($"{prefix}wipe-db");
                response.AppendLine($"{prefix}add-currency    Slug [@Professions] Description");
                response.AppendLine($"{prefix}update-currency #slug NewSlug [@Professions] Description");
                response.AppendLine($"{prefix}professions     [@Professions]```");
            }

            return ReplyAsync(response.ToString());
        }


        [Command("инфо")]
        public Task InfoRu(string arg = null)
        {
            return SendInfoRu(arg);
        }

        [Command("помощь")]
        public Task HelpRu(string arg = null)
        {
            return SendInfoRu(arg);
        }

        [Command("комманды")]
        public Task CommandsRu(string arg = null)
        {
            return SendInfoRu(arg);
        }

        // todo: auto-generate from attributes
        private Task<IUserMessage> SendInfoRu(string arg)
        {
            var response = new ResponseBuilder();
            var currencies = string.Join(", ", _currencies.GetAll().Select(x => x.Slug));
            var settings = _settings.FirstOrDefault();
            var prefix = settings?.Prefix;

            if (string.IsNullOrEmpty(prefix))
                prefix = $"{prefix}";

            response.AppendLine("@ требует упоминание, [] это опциональный параметр, # это идентификатор (например, 5b20466f7e4440132c6c3c63)");
            response.AppendLine();
            response.AppendLine($"```Валюты:    {currencies}");
            response.AppendLine($"{prefix}награды   [@Игрок]");
            response.AppendLine($"{prefix}квесты    [show-ids]");
            response.AppendLine($"{prefix}титулы    [@Игрок]");
            response.AppendLine($"{prefix}сдачи     [@Игрок]");
            response.AppendLine($"{prefix}крафтеры  @Профессия");
            response.AppendLine($"{prefix}заказы    [@Игрок] [@Профессия]");
            response.AppendLine($"{prefix}обмен                                            - Курс обмена валют");
            response.AppendLine($"{prefix}обменять  Кол-во Валюта1 Валюта2                 - Обменять Кол-во Валюты1 на Валюту2");
            response.AppendLine($"{prefix}профессия @Профессия                             - Назначить/убрать свою профессию.");
            response.AppendLine($"{prefix}заказ     @Профессия [Кол-во] [Валюта] Описание  - Заказать крафт, заплатив Кол-во Валюты");
            response.AppendLine($"{prefix}отменить  #IdЗаказа                              - Отменить крафт или заказ");
            response.AppendLine($"{prefix}скрафчу   #IdЗаказа                              - Назначить себя отвественным за крафт");
            response.AppendLine($"{prefix}скрафтил  @Профессия                             - Показывает сколько заказов ты скрафтил");
            response.AppendLine($"{prefix}скрафтил  #IdЗаказа                              - Пометить заказ как скрафченный");
            response.AppendLine($"Дополнительно: Опубликуй скриншот с командой {prefix}скрафтил в описании, чтобы сохранить скриншот.```");

            // todo: refactor to right place
            if (settings != null)
            {
                if (_guildService.IsUserInRole(ModuleContext.MentionedOrCurrentUser, settings.Banker))
                {
                    response.AppendLine();
                    response.AppendLine($"```Сдача квестов:");
                    response.AppendLine($"{prefix}курс  Кол-во1 Валюта1 Кол-во2 Валюта2");
                    response.AppendLine($"{prefix}всего");
                    response.AppendLine($"{prefix}сдать #IdКвеста Кол-во-валюты Валюта @Игрок [@ЕщеИгроки]");
                    response.AppendLine($"Дополнительно: Опубликуй скриншот с командой {prefix}сдать в описании, чтобы сохранить скриншот.```");
                }

                if (_guildService.IsUserInRole(ModuleContext.MentionedOrCurrentUser, settings.Quester))
                {
                    response.AppendLine();
                    response.AppendLine($"```Управление квестами (Тип: harvest, craft, pvp):");
                    response.AppendLine($"{prefix}добавить-квест       -n Название -t Тип -a Кол-во-валюты -c Валюта -d Описание ");
                    response.AppendLine($"{prefix}редактировать-квест  #IdКвеста -n Название -t Тип -a Кол-во -c Валюта -d Описание");
                    response.AppendLine($"{prefix}выключить-квест      #IdКвеста");
                    response.AppendLine($"{prefix}включить-квест       #IdКвеста");
                    response.AppendLine($"{prefix}добавить-титул       #IdКвеста Кол-во Название```");
                }
            }

            if (_guildService.IsUserAdmin(ModuleContext.MentionedOrCurrentUser.Id))
            {
                response.AppendLine();
                response.AppendLine("```Настройки бота:");
                response.AppendLine($"{prefix}квестоплет           @Роль");
                response.AppendLine($"{prefix}банкир               @Роль");
                response.AppendLine($"{prefix}префикс              строка");
                response.AppendLine($"{prefix}канал                #канал");
                response.AppendLine($"{prefix}стереть-бд");
                response.AppendLine($"{prefix}добавить-валюту      Id [@Профессия] Описание");
                response.AppendLine($"{prefix}редактировать-валюту #IdВалюты НовыйId [@Профессия] Описание");
                response.AppendLine($"{prefix}профессии            [@Профессии]```");
            }

            return ReplyAsync(response.ToString());
        }
    }
}