using System;

namespace WebAPI.Attributes
{
    class SlugAttribute : Attribute
    {
        public string Sign { get; }

        public SlugAttribute(string sign)
        {
            Sign = sign;
        }
    }
}