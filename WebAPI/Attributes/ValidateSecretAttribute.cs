﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using WebAPI.Services;

namespace WebAPI.Attributes
{
    public class ValidateSecretAttribute : ActionFilterAttribute
    {
        private const string Secret = "secret";

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var variables = context.HttpContext.RequestServices.GetService<ISettingsService>();
            var maintenanceSecret = variables.MaintenanceSecret;
            var secret = context.ActionArguments.ContainsKey(Secret) ? context.ActionArguments[Secret] : null;

            if (string.IsNullOrEmpty(maintenanceSecret) || secret as string != maintenanceSecret)
            {
                context.Result = new NotFoundResult();
            }
            else
            {
                base.OnActionExecuting(context);
            }
        }
    }
}