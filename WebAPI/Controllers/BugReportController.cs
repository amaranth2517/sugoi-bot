﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Forms;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/bug-report")]
    [ApiController]
    public class BugReportController : Controller
    {
        private readonly IMailService _mailService;

        public BugReportController(IMailService mailService)
        {
            _mailService = mailService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] BugReportForm form)
        {
            var bodyBuilder = new StringBuilder();
            bodyBuilder.AppendLine($"Version: {form.Version}");
            bodyBuilder.AppendLine($"Character: {form.Character}");
            bodyBuilder.AppendLine($"Discord handle: {form.DiscordHandle}");
            bodyBuilder.AppendLine($"Comment: {form.Comment}");
            bodyBuilder.AppendLine($"System Info: {form.SystemInfo}");

            var attachments = new List<Attachment>();
            if (form.Log1 != null)
                attachments.Add(GetFile(form.Log1));
            if (form.Log2 != null)
                attachments.Add(GetFile(form.Log2));
            if (form.Log3 != null)
                attachments.Add(GetFile(form.Log3));
            if (form.Log4 != null)
                attachments.Add(GetFile(form.Log4));
            if (form.Log5 != null)
                attachments.Add(GetFile(form.Log5));

            return await _mailService.SendAsync("Parser bug report", bodyBuilder.ToString(), attachments) ? Ok() : StatusCode(500);
        }

        private static Attachment GetFile(IFormFile file)
        {
            using var stream = file.OpenReadStream();
            var contents = new byte[file.Length];
            stream.Read(contents);
            return new Attachment
            {
                Data = Convert.ToBase64String(contents),
                Name = file.FileName,
                Type = "application/text"
            };
        }
    }
}