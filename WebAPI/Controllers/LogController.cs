﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Attributes;

namespace WebAPI.Controllers
{
    [Route("api/log")]
    [ApiController]
    [ValidateSecret]
    public class LogController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Get([Required] string secret)
        {
            return Ok(await System.IO.File.ReadAllTextAsync("/Logs/ServerLog.log"));
        }

        [HttpGet("{order}")]
        public async Task<IActionResult> Get([Required] string secret, int order)
        {
            return Ok(await System.IO.File.ReadAllTextAsync($"/Logs/ServerLog.{order}.log"));
        }
    }
}