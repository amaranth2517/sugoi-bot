﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord.WebSocket;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Database;
using WebAPI.Forms;
using WebAPI.Models;
using WebAPI.Services;

namespace WebAPI.Controllers
{
    [Route("api/publish")]
    [ApiController]
    // [ValidateSecret]
    public class PublishController : Controller
    {
        private readonly DiscordSocketClient _discordClient;
        private readonly ISettingsService _settingsService;
        private readonly InstallerRepository _installerRepository;

        public PublishController(DiscordSocketClient discordClient, ISettingsService settingsService, IRepository<Installer> installerRepository)
        {
            _discordClient = discordClient;
            _settingsService = settingsService;
            _installerRepository = (InstallerRepository) installerRepository;
        }

        // todo: add secret
        [HttpPost("{type}")]
        public async Task Publish(string type, [FromForm] PublishForm form)
        {
            var amatalkDiscord = _discordClient.Guilds.FirstOrDefault(x => x.Id == _settingsService.AmatalkId);

            var channelId = type switch
            {
                "release" => _settingsService.ReleaseChannelId,
                "beta" => _settingsService.BetaChannelId,
                "alpha" => _settingsService.AlphaChannelId,
                _ => throw new Exception("Bad Type")
            };

            var channel = amatalkDiscord?.Channels.OfType<SocketTextChannel>().FirstOrDefault(x => x.Id == channelId);
            if (channel != null)
            {
                var result = await channel.SendFileAsync(form.File.OpenReadStream(), form.File.FileName, "New Crowbars version was released!");
                var url = result.Attachments.FirstOrDefault()?.Url;
                var regex = new Regex(".*v(.*)\\.exe");
                var versionString = regex.Match(form.File.FileName).Groups[1].Value;
                if (Version.TryParse(versionString, out var version))
                    _installerRepository.Add(new Installer {Branch = type, Url = url, Version = version});
            }
        }

        [HttpGet("new-version/{type}/{version}")]
        public string GetUpdatedVersion(string type, string version)
        {
            if (!Version.TryParse(version, out var clientVersion))
                return null;

            var newInstaller = _installerRepository.GetAll(x => x.Branch == type && x.Version > clientVersion).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
            return newInstaller?.Url;
        }
    }
}