﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Database;
using WebAPI.Hubs;

namespace WebAPI.Controllers
{
    [Route("api/raids")]
    [ApiController]
    public class RaidController : Controller
    {
        private readonly IRepository<Raid> _raids;

        public RaidController(IRepository<Raid> raids)
        {
            _raids = raids;
        }

        [HttpGet("{handle}/{index}")]
        public IActionResult Get(string handle, int index)
        {
            var raid = _raids.Get(x => x.Handle == handle);
            var fight = raid?.Fights.ElementAtOrDefault(index);

            return fight == null ? (IActionResult) NotFound() : Ok(fight.Characters);
        }
        
        [HttpGet("{handle}/fights-count")]
        public IActionResult FightsCount(string handle)
        {
            var raid = _raids.Get(x => x.Handle == handle);

            return raid == null ? (IActionResult) NotFound() : Ok(raid.Fights.Count);
        }
    }
}