﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPI.Models
{
    public class Request : IEntity, ITrackedEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("CreatedAt")] public DateTime CreatedAt { get; set; }
        [BsonElement("UpdatedAt")] public DateTime UpdatedAt { get; set; }
        [BsonElement("UserId")] public ulong PlayerId { get; set; }
        [BsonElement("CrafterId")] public ulong CrafterId { get; set; }
        [BsonElement("ProfessionId")] public ulong ProfessionId { get; set; }
        [BsonElement("Description")] public string Description { get; set; }
        [BsonElement("State")] public CraftingState State { get; set; }
        [BsonElement("Screenshot")] public string Screenshot { get; set; }
        [BsonElement("Amount")] public int Amount { get; set; }
        [BsonElement("CurrencyId")] public ObjectId CurrencyId { get; set; }
    }
}