using System.Collections.Generic;

namespace WebAPI.Models
{
    public class RaidFight 
    {
        public Skill DamageDone { get; set; }
        public Skill DamageReceived { get; set; }
        public Skill HealingDone { get; set; }
        public Skill HealingReceived { get; set; }
        public IEnumerable<Opponent> Opponents { get; set; }
    }
}