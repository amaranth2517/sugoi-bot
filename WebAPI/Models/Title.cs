﻿namespace WebAPI.Models
{
    public class Title
    {
        public int Completions { get; set; }
        public string Name { get; set; }
    }
}