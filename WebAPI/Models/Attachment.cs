namespace WebAPI.Models
{
    public class Attachment
    {
        public string Type { get; set; }

        public string Name { get; set; }

        public string Data { get; set; }
    }
}