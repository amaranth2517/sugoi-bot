﻿using System.ComponentModel;

namespace WebAPI.Models
{
    public enum CraftingState
    {
        [Description("Requested")] Requested,
        [Description("Crafting")] Crafting,
        [Description("Crafted")] Crafted
    }
}