﻿using System.ComponentModel;
using WebAPI.Attributes;

namespace WebAPI.Models
{
    public enum QuestType
    {
        None = -1,

        [Description("Harvesting"), Slug("harvest")]
        Harvesting = 0,

        [Description("Crafting"), Slug("craft")]
        Crafting = 1,

        [Description("Pvp"), Slug("pvp")] Pvp = 2,
    }
}