using System.ComponentModel;
using WebAPI.Attributes;

namespace WebAPI.Models
{
    public enum RewardType
    {
        [Description("gear point(-s)"), Slug("gp")]
        GearPoints,

        [Description("vessel point(-s)"), Slug("vp")]
        VesselPoints,

        [Description("jewelry point(-s)"), Slug("jp")]
        JewelryPoints,
        [Description("coin(-s)"), Slug("c")] Coins
    }
}