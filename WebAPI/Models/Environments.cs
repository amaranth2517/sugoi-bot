namespace WebAPI.Models
{
    internal static class Environments
    {
        public const string Development = "Development";
        public const string Production = "Production";
    }
}