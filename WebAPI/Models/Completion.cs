﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebAPI.Database;

namespace WebAPI.Models
{
    public class Completion : IEntity, IHavePlayer, IVersion, ITrackedEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("CreatedAt")] public DateTime CreatedAt { get; set; }
        [BsonElement("UpdatedAt")] public DateTime UpdatedAt { get; set; }
        [BsonElement("PlayerId")] public ulong PlayerId { get; set; }
        [BsonElement("QuestId")] public ObjectId QuestId { get; set; }
        [BsonElement("Amount")] public float Amount { get; set; }
        [BsonElement("RewardType")] public RewardType RewardType { get; set; }
        [BsonElement("CurrencyId")] public ObjectId CurrencyId { get; set; }
        [BsonElement("Screenshot")] public string Screenshot { get; set; }
        [BsonElement("HelpersIds")] public List<ulong> HelpersIds { get; set; }
        [BsonElement("Version")] public int Version { get; set; }
    }
}