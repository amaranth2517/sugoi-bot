namespace WebAPI.Models
{
    public sealed class Skill
    {
        public int PerSecond { get; set; }
        public int Total { get; set; }
        public int Hits { get; set; }
        public int Crits { get; set; }
    }
}