﻿namespace WebAPI.Models
{
    public interface IVersion
    {
        int Version { get; set; }
    }
}