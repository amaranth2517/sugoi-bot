﻿namespace WebAPI.Models
{
    public static class Responses
    {
        public static readonly Response MissingProfession = new Response(1, "Enter profession with @ character.", "Нужно ввести професиию с символом @.");
        public static readonly Response MissingDescription = new Response(2, "Enter description for crafter.", "Нужно ввести описание заказа.");
        public static readonly Response NotFound = new Response(3, "Record with such Id does not exist.", "Запись с таким идентификатором не найдена.");
        public static readonly Response Unauthorized = new Response(4, "You can't do that.", "Неа.");
        public static readonly Response AlreadyCrafting = new Response(5, "{0} is already crafting this item.", "{0} уже крафтит это заказ.");
        public static readonly Response RequestCanceled = new Response(6, "The request was cancelled. *{0} {1} got added to your rewards.*", "Заказ отменён. *{0} {1} добавлены к вашим наградам.*");
        public static readonly Response NotCrafting = new Response(7, "Request is not in crafting state.", "Заказ ещё не крафтится.");

        public static readonly Response CraftingCancelled = new Response(8, "{0}, crafting was cancelled for request {1}. {2}, please grab this request.\n*Description: {3}*",
            "{0}, крафт был отменён для заказа {1}. {2}, пожалуйста, возьмите этот заказ.\n*Описание: {3}*");

        public static readonly Response WillCraft = new Response(9, "{0}, {1} will complete request {2} for you.\n*Description: {3}*", "{0}, {1} скрафтит ваш заказ {2}.\n*Описание: {3}*");

        public static readonly Response Crafted = new Response(10, "{0}, your item is crafted! Pick it from {1}.\n*Description: {2}*",
            "{0}, ваш предмет скрафчен! Заберите его у {1}.\n*Описание: {2}*");

        public static readonly Response DatabaseError = new Response(11, "Error updating document. Contact developer.", "Ошибка сохранения документа. Обратитесь к разработчику.");

        public static readonly Response NotEnoughPoints = new Response(12, "You don't have enough points to request crafting. **convert** command can help you to convert currencies.",
            "У вас не хватает очков для заказа этого крафта. У помощью комманды **обменять** можно обменять валюты.");

        public static readonly Response RequestCreated =
            new Response(13, "The request {0} was created. *{1} {2} got removed from your rewards.*", "Заказ {0} создан.*{1} {2} вычтен(-ы) из ваших наград.*");

        public static readonly Response QuestAdded = new Response(14, "Quest {0} was added with slug **{1}**.", "Квест {0} создан с Id **{1}**.");
        public static readonly Response AmountIsNotNumber = new Response(15, "Amount is not a number.", "Указанное Кол-во - не число.");
        public static readonly Response InvalidRewardType = new Response(16, "We don't have such currency.", "Нет такой валюты.");
        public static readonly Response InvalidQuestType = new Response(17, "We don't have such quest type.", "Нет такого типа квеста.");
        public static readonly Response QuestUpdated = new Response(19, "Quest {0} was updated.", "Квест {0} отредактирован.");
        public static readonly Response QuestDisabled = new Response(20, "Quest {0} was disabled.", "Квест {0} выключен.");
        public static readonly Response QuestEnabled = new Response(21, "Quest {0} was enabled.", "Квест {0} включён.");
        public static readonly Response TitleAdded = new Response(22, "Title {0} was added to {1}.", "Титул {0} добавлен в {1}.");
        public static readonly Response TurnedIn = new Response(23, "{0} got {1:F2} {2}.", "{0} получил(-и) {1:F2} {2}.");
        public static readonly Response NoRewards = new Response(24, "{0} has no rewards.", "У {0} нет наград.");
        public static readonly Response NoTitles = new Response(25, "Player does not have any titles.", "У игрока нет титулов.");
        public static readonly Response MissingPlayers = new Response(26, "Specify players.", "Укажите игроков.");
        public static readonly Response YouCrafted = new Response(27, "{0} crafted {1} item(-s) with {2}.", "{0} скрафтил {1} предмет(-ов) в {2}.");

        public static readonly Response DuplicateSlug = new Response(28, "Quest slug {0} will collide with another quest {1}. Pick another name.",
            "Id квеста {0} совпадает с другим квестом {1}. Выберите другое назнание.");

        public static readonly Response Converted = new Response(29, "{0} {1} got converted to {2} {3}.", "{0} {1} обменены на {2} {3}.");
        public static readonly Response RateSet = new Response(30, "Exchange rate was set.", "Курс обмена задан.");
        public static readonly Response UnknownCurrency = new Response(31, "Unknown currency: {0}", "Неизвестная валюта: {0}");
        public static readonly Response NotEnoughCurrency = new Response(32, "You don't have enough currency.", "У вас не хватает очков.");

        public static readonly Response ExchangeNotAllowed =
            new Response(33, "Such exchange is not allowed. **rates** for more info.", "Данный обмен не возможен. Посмотрите доступные курс с помощью комманды **курс-обмена**.");

        public static readonly Response Assigned = new Response(34, "{0} tag was assigned to you.", "Вам назначен тэг {0}.");
        public static readonly Response Removed = new Response(35, "{0} tag was removed from you.", "{0} тэг был убран.");
        public static readonly Response QuesterSet = new Response(36, "Quester role was set.", "Роль квестоплета задана.");
        public static readonly Response BankerSet = new Response(37, "Banker role was set.", "Роль банкира задана.");
        public static readonly Response PrefixSet = new Response(38, "Prefix role was set.", "Префикс задан.");
        public static readonly Response ProfessionsSet = new Response(39, "Professions were set.", "Профессии заданы.");
        public static readonly Response CurrencyAdded = new Response(40, "Currency was added.", "Валюта добавлена.");
        public static readonly Response CurrencyUpdated = new Response(41, "Currency was updated.", "Валюта отредактирована.");
        public static readonly Response Noone = new Response(42, "Noone.", "Тут никого.");
        public static readonly Response AmountIsNegative = new Response(43, "Amount should be positive.", "Кол-во должно быть положительным.");

        public static readonly Response WriteTokenToConfirm = new Response(44,
            "Attention! You are going to delete all records for completions, rewards and requests. Quests and Currencies will stay. This action can't be reverted! To confirm your action, write this command again with ```{0}``` parameter. Parameter is valid for 2 minutes.",
            "Внимание! Вы собираетесь удалить все записи о сдачах, наградах и заказах. Квесты и валюты останутся.Это действие необратимо! Для подтверждения, напишите эту команду еще раз с параметром ```{0}```. Параметр действителен в течение 2-х минут.");

        public static readonly Response DatabaseWiped = new Response(45, "Completions, rewards and requests were wiped.", "Сдачи, награды и заказы были стёрты.");
        public static readonly Response ChannelSet = new Response(46, "Bot channel was set.", "Канал бота был задан.");
        public static readonly Response CurrencyDeleted = new Response(47, "Currency was deleted.", "Валюта удалена.");
    }
}