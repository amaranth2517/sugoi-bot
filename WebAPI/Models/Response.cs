﻿namespace WebAPI.Models
{
    public class Response
    {
        private int StatusCode { get; }
        public string Message { get; }
        public string MessageRu { get; }

        public Response(int statusCode, string message, string messageRu = null)
        {
            StatusCode = statusCode;
            Message = message;
            MessageRu = messageRu;
        }

        public Response(string message)
        {
            StatusCode = 0;
            Message = message;
        }

        public Response Params(params object[] args)
        {
            return new Response(StatusCode, string.Format(Message, args), string.Format(MessageRu, args));
        }

        public override bool Equals(object obj)
        {
            return ((Response) obj).StatusCode == StatusCode;
        }

        public override int GetHashCode()
        {
            return StatusCode.GetHashCode();
        }
    }
}