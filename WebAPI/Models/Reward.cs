﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebAPI.Database;

namespace WebAPI.Models
{
    // Note: table exists and is denormalised for performance reasons
    public class Reward : IEntity, IHavePlayer, IVersion, ITrackedEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("CreatedAt")] public DateTime CreatedAt { get; set; }
        [BsonElement("UpdatedAt")] public DateTime UpdatedAt { get; set; }
        [BsonElement("PlayerId")] public ulong PlayerId { get; set; }
        [BsonElement("GearPoints")] public float GearPoints { get; set; }
        [BsonElement("VesselPoints")] public float VesselPoints { get; set; }
        [BsonElement("JewelryPoints")] public float JewelryPoints { get; set; }
        [BsonElement("Coins")] public float Coins { get; set; }
        [BsonElement("Version")] public int Version { get; set; }
        [BsonElement("Points")] public Dictionary<string, float> Points { get; set; }
    }
}