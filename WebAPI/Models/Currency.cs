﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebAPI.Database;

namespace WebAPI.Models
{
    public class Currency : IEntity, IHaveSlug
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("Slug")] public string Slug { get; set; }
        [BsonElement("Description")] public string Description { get; set; }
        [BsonElement("Professions")] public List<ulong> Professions { get; set; }
    }
}