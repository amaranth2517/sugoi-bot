﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPI.Models
{
    public class Setting : IEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("Prefix")] public string Prefix { get; set; }
        [BsonElement("Quester")] public ulong Quester { get; set; }
        [BsonElement("Banker")] public ulong Banker { get; set; }
        [BsonElement("Professions")] public List<ulong> Professions { get; set; }
        [BsonElement("WipeAt")] public DateTime WipeAt { get; set; }
        [BsonElement("WipeToken")] public string WipeToken { get; set; }
        [BsonElement("ChannelId")] public ulong ChannelId { get; set; }
        [BsonElement("Version")] public string Version { get; set; }
    }
}