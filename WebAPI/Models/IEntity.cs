﻿using MongoDB.Bson;

namespace WebAPI.Models
{
    public interface IEntity
    {
        ObjectId Id { get; set; }
    }
}