namespace WebAPI.Models
{
    public class Opponent
    {
        public int DamageDone { get; set; }
        public int DamageReceived { get; set; }
        public string Name { get; set; }
    }
}