﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPI.Models
{
    public class Installer : IEntity, ITrackedEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        
        [BsonElement("CreatedAt")] public DateTime CreatedAt { get; set; }
        [BsonElement("UpdatedAt")] public DateTime UpdatedAt { get; set; }
        [BsonElement("Url")] public string Url { get; set; }
        [BsonElement("Branch")] public string Branch { get; set; }
        [BsonElement("Version")] public Version Version { get; set; }
    }
}