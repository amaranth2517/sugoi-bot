namespace WebAPI.Models
{
    public class Character
    {
        public string Name { get; set; }

        public RaidFight Fight { get; set; }

    }
}