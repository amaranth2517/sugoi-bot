﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebAPI.Models
{
    public class Rate : IEntity, IVersion
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("SourceAmount")] public float SourceAmount { get; set; }
        [BsonElement("SourceCurrency")] public RewardType SourceCurrency { get; set; }
        [BsonElement("SourceCurrencyId")] public ObjectId SourceCurrencyId { get; set; }
        [BsonElement("TargetAmount")] public float TargetAmount { get; set; }
        [BsonElement("TargetCurrency")] public RewardType TargetCurrency { get; set; }
        [BsonElement("TargetCurrencyId")] public ObjectId TargetCurrencyId { get; set; }
        [BsonElement("Version")] public int Version { get; set; }
    }
}