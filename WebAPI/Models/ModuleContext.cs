﻿using System.Collections.Generic;
using System.Linq;
using Discord;

namespace WebAPI.Models
{
    public class ModuleContext : IContext
    {
        public IEnumerable<IRole> MentionedRoles { get; }
        public IEnumerable<IUser> MentionedUsers { get; }
        public IUser User { get; }
        public IEnumerable<IAttachment> Attachments { get; }
        public IEnumerable<IChannel> MentionedChannels { get; }

        public IUser MentionedOrCurrentUser
        {
            get
            {
                var user = User;
                var mentionedUser = MentionedUsers.FirstOrDefault();

                if (mentionedUser != null)
                {
                    user = mentionedUser;
                }

                return user;
            }
        }

        public ModuleContext(IUser user, List<IRole> roles, List<IUser> users, List<IChannel> channels)
        {
            User = user;
            MentionedRoles = roles;
            MentionedUsers = users;
            MentionedChannels = channels;
        }

        public ModuleContext(IUser user, List<IRole> roles)
        {
            User = user;
            MentionedRoles = roles;
            MentionedUsers = new List<IUser>();
            MentionedChannels = new List<IChannel>();
        }

        public ModuleContext(IUser user, IRole role)
        {
            User = user;
            MentionedRoles = new List<IRole> {role};
            MentionedUsers = new List<IUser>();
            MentionedChannels = new List<IChannel>();
        }

        public ModuleContext(IUser user, IRole role, IUser mentionedUser)
        {
            User = user;
            MentionedRoles = new List<IRole> {role};
            MentionedUsers = new List<IUser> {mentionedUser};
            MentionedChannels = new List<IChannel>();
        }

        public ModuleContext(IUser user)
        {
            User = user;
            MentionedRoles = new List<IRole>();
            MentionedUsers = new List<IUser>();
            MentionedChannels = new List<IChannel>();
        }

        public ModuleContext(IUser user, IAttachment attachment)
        {
            User = user;
            MentionedRoles = new List<IRole>();
            MentionedUsers = new List<IUser>();
            MentionedChannels = new List<IChannel>();
            Attachments = new List<IAttachment> {attachment};
        }

        public ModuleContext(IUser user, IEnumerable<IRole> roles, IEnumerable<IUser> users, IEnumerable<IAttachment> attachments, IEnumerable<IChannel> channels)
        {
            User = user;
            MentionedRoles = roles;
            MentionedUsers = users;
            MentionedChannels = channels;
            Attachments = attachments;
        }

        public ModuleContext(IUser user, IUser mentionedUser)
        {
            User = user;
            MentionedRoles = new List<IRole>();
            MentionedUsers = new List<IUser> {mentionedUser};
            Attachments = new List<IAttachment>();
            MentionedChannels = new List<IChannel>();
        }
    }
}