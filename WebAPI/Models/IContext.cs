﻿using System.Collections.Generic;
using Discord;

namespace WebAPI.Models
{
    public interface IContext
    {
        IEnumerable<IRole> MentionedRoles { get; }
        IEnumerable<IUser> MentionedUsers { get; }
        IUser User { get; }
        IUser MentionedOrCurrentUser { get; }
        IEnumerable<IAttachment> Attachments { get; }
        IEnumerable<IChannel> MentionedChannels { get; }
    }
}