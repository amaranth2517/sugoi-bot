﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using WebAPI.Database;

namespace WebAPI.Models
{
    public class Quest : IEntity, IHaveSlug, IVersion, ITrackedEntity
    {
        [BsonId] public ObjectId Id { get; set; }
        [BsonElement("CreatedAt")] public DateTime CreatedAt { get; set; }
        [BsonElement("UpdatedAt")] public DateTime UpdatedAt { get; set; }
        [BsonElement("Name")] public string Name { get; set; }
        [BsonElement("Slug")] public string Slug { get; set; }
        [BsonElement("QuestType")] public QuestType QuestType { get; set; }
        [BsonElement("Description")] public string Description { get; set; }
        [BsonElement("Amount")] public float Amount { get; set; }
        [BsonElement("RewardType")] public RewardType RewardType { get; set; }
        [BsonElement("CurrencyId")] public ObjectId CurrencyId { get; set; }
        [BsonElement("Active")] public bool Active { get; set; }
        [BsonElement("Titles")] public List<Title> Titles { get; set; }
        [BsonElement("Version")] public int Version { get; set; }
    }
}