using System;

namespace WebAPI.Models
{
    public interface ITrackedEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}