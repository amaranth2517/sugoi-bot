﻿using System;
using System.Linq;
using MongoDB.Bson;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class CraftingService
    {
        private readonly IRepository<Reward> _rewards;
        private readonly IRepository<Request> _requests;
        private readonly IRepository<Setting> _settings;
        private readonly IRepository<Currency> _currencies;
        private readonly IGuildService _guildService;

        public CraftingService(IGuildService guildService, IRepository<Reward> rewards, IRepository<Request> requests, IRepository<Setting> settings,
            IRepository<Currency> currencies)
        {
            _rewards = rewards;
            _requests = requests;
            _settings = settings;
            _currencies = currencies;
            _guildService = guildService;
        }

        public Response Request(IContext context, string professionRaw, params string[] args)
        {
            var profession = context.MentionedRoles.FirstOrDefault();
            if (profession == null)
                return Responses.MissingProfession;

            if (!_settings.FirstOrDefault().Professions.Contains(profession.Id))
                return Responses.Unauthorized;

            if (!args.Any())
                return Responses.MissingDescription;

            string description;
            Currency currency;
            if (int.TryParse(args[0], out var amount))
            {
                if (amount < 0)
                    return Responses.AmountIsNegative;

                currency = _currencies.GetBySlug(args[1]);
                if (currency == null)
                {
                    currency = _currencies.GetAll().FirstOrDefault(x => x.Professions.Contains(profession.Id));

                    if (currency == null)
                        return Responses.UnknownCurrency.Params(args[1]);

                    description = string.Join(" ", args.Skip(1));
                }
                else
                {
                    description = string.Join(" ", args.Skip(2));
                }
            }
            else
            {
                currency = _currencies.GetAll().FirstOrDefault(x => x.Professions.Contains(profession.Id));
                if (currency == null)
                    return Responses.UnknownCurrency.Params(args[1]);

                amount = 1;
                description = string.Join(" ", args);
            }

            if (string.IsNullOrEmpty(description))
                return Responses.MissingDescription;

            var record = _rewards.GetAll().FirstOrDefault(x => x.PlayerId == context.User.Id);

            if (record == null || !record.HasRewards(currency, _currencies) || record.Points[currency.Id.ToString()] < amount)
                return Responses.NotEnoughPoints;

            record.UpdateRewards(currency, -amount, _currencies);

            if (!_rewards.Update(record))
                return Responses.DatabaseError;

            var request = new Request
            {
                PlayerId = context.User.Id,
                ProfessionId = profession.Id,
                State = CraftingState.Requested,
                Description = description,
                Amount = amount,
                CurrencyId = currency.Id
            };
            _requests.Add(request);

            return Responses.RequestCreated.Params(request.Id, amount, currency.Description);
        }

        public Response Requests(IContext context)
        {
            var response = new ResponseBuilder();

            if (context.MentionedUsers.Any() || !context.MentionedRoles.Any())
            {
                GetPlayersRequests(context, response, CraftingState.Requested);
                GetPlayersRequests(context, response, CraftingState.Crafted);
                response.AppendLine();
            }

            GetProfessionsRequests(context, response, CraftingState.Requested);
            GetProfessionsRequests(context, response, CraftingState.Crafting);

            return new Response(response.ToString());
        }

        private void GetProfessionsRequests(IContext context, ResponseBuilder response, CraftingState state)
        {
            foreach (var profession in context.MentionedRoles.Distinct())
            {
                var professionRequests = _requests.GetAll().Where(x => x.ProfessionId == profession.Id && x.State == state).OrderByDescending(x => x.CreatedAt).ToList();

                if (professionRequests.Any())
                {
                    response.AppendLine($"**{profession.Name} {state.Print()} items:**");
                    foreach (var request in professionRequests)
                    {
                        var crafter = _guildService.GetUser(request.CrafterId)?.GetUsername();
                        var ending = string.IsNullOrEmpty(crafter) ? "" : ", crafter: " + crafter;
                        var currencySlug = (_currencies.Get(request.CurrencyId) ?? _currencies.GetAll().FirstOrDefault(x => x.Professions.Contains(request.ProfessionId)))?.Description;
                        response.AppendLine(
                            $"{request.CreatedAt.ToShortDateString()}: {request.Amount} {currencySlug}, *{request.Description}* ({request.Id} by {_guildService.GetUser(request.PlayerId)?.GetUsername() + ending}).");
                    }

                    response.AppendLine();
                }
                else
                {
                    response.AppendLine($"**Nothing is {state.Print()} for {profession.Name}.**");
                }
            }
        }

        private void GetPlayersRequests(IContext context, ResponseBuilder response, CraftingState state)
        {
            var user = context.MentionedOrCurrentUser;
            var username = user.GetUsername();

            var playerRequests = _requests.GetAll().Where(x => x.PlayerId == user.Id && x.State == state).OrderByDescending(x => x.CreatedAt).ToList();

            if (playerRequests.Any())
            {
                response.AppendLine($"**{username}'s {state.Print()} items:**");
                foreach (var request in playerRequests)
                {
                    var crafter = _guildService.GetUser(request.CrafterId)?.GetUsername();
                    var ending = string.IsNullOrEmpty(crafter) ? request.Id.ToString() : "by " + crafter;
                    var currencySlug = (_currencies.Get(request.CurrencyId) ?? _currencies.GetAll().FirstOrDefault(x => x.Professions.Contains(request.ProfessionId)))?.Description;
                    response.AppendLine(
                        $"{request.CreatedAt.ToShortDateString()}: {request.Amount} {currencySlug}, *{request.Description}* ({ending}, {_guildService.GetRole(request.ProfessionId)?.Name})");
                }

                response.AppendLine();
            }
            else
            {
                response.AppendLine($"**Nothing is {state.Print()}.**");
            }
        }

        public Response Cancel(IContext context, string id)
        {
            var record = _requests.Get(ObjectId.Parse(id));
            if (record == null)
                return Responses.NotFound;

            return record.CrafterId == context.User.Id ? CancelCrafting(context, record) : CancelRequest(context, record);
        }

        private Response CancelRequest(IContext context, Request request)
        {
            if (request.PlayerId != context.User.Id)
                return Responses.Unauthorized;

            if (request.State != CraftingState.Requested)
                return Responses.AlreadyCrafting.Params(_guildService.GetUser(request.CrafterId).Mention);

            var currency = _currencies.Get(request.CurrencyId) ?? _currencies.GetAll().FirstOrDefault(x => x.Professions.Contains(request.ProfessionId));

            if (request.Amount > 0)
            {
                if (currency == null)
                    return Responses.DatabaseError;

                var record = _rewards.GetAll().FirstOrDefault(x => x.PlayerId == context.User.Id);
                if (record == null)
                    return Responses.DatabaseError;

                record.UpdateRewards(currency, request.Amount, _currencies);

                if (!_rewards.Update(record))
                    return Responses.DatabaseError;
            }
            else
            {
                currency = _currencies.FirstOrDefault();
            }

            return _requests.Delete(request.Id)
                ? Responses.RequestCanceled.Params(request.Amount, currency.Description)
                : Responses.DatabaseError;
        }

        private Response CancelCrafting(IContext context, Request record)
        {
            if (record.CrafterId != context.User.Id)
                return Responses.Unauthorized;

            if (record.State != CraftingState.Crafting)
                return Responses.NotCrafting;

            record.CrafterId = 0;
            record.State = CraftingState.Requested;

            return _requests.Update(record)
                ? Responses.CraftingCancelled.Params(_guildService.GetUser(record.PlayerId).Mention, record.Id, _guildService.GetRole(record.ProfessionId).Mention, record.Description)
                : Responses.DatabaseError;
        }

        public Response Crafting(IContext context, string id)
        {
            var record = _requests.Get(ObjectId.Parse(id));
            if (record == null)
                return Responses.NotFound;

            if (!_guildService.IsUserInRole(context.User, record.ProfessionId))
                return Responses.Unauthorized;

            if (record.State != CraftingState.Requested)
                return Responses.AlreadyCrafting.Params(_guildService.GetUser(record.CrafterId).Mention);

            record.CrafterId = context.User.Id;
            record.State = CraftingState.Crafting;

            var result = _requests.Update(record);

            return result
                ? Responses.WillCraft.Params(_guildService.GetUser(record.PlayerId).Mention, context.User, record.Id, record.Description)
                : Responses.DatabaseError;
        }

        public Response Crafted(IContext context, string id, string dummy = null)
        {
            var profession = context.MentionedRoles.FirstOrDefault();
            if (profession != null)
            {
                var user = context.MentionedOrCurrentUser;
                var count = _requests.GetAll().Where(x => x.CrafterId == user.Id && x.State == CraftingState.Crafted && x.ProfessionId == profession.Id).Sum(x => x.Amount > 0 ? x.Amount : 1);

                return Responses.YouCrafted.Params(user.GetUsername(), count, profession.Name);
            }

            var record = _requests.Get(ObjectId.Parse(id));
            if (record == null)
                return Responses.NotFound;

            if (record.State == CraftingState.Crafting && record.CrafterId != context.User.Id)
                return Responses.Unauthorized;

            if (record.State == CraftingState.Crafted)
                return Responses.NotCrafting;

            record.State = CraftingState.Crafted;
            record.Screenshot = context.Attachments.FirstOrDefault()?.Url;

            var result = _requests.Update(record);

            return result
                ? Responses.Crafted.Params(_guildService.GetUser(record.PlayerId).Mention, context.User.Mention, record.Description)
                : Responses.DatabaseError;
        }

        public Response Crafters(IContext context)
        {
            var profession = context.MentionedRoles.FirstOrDefault();

            if (profession == null)
                return Responses.NotFound;

            var users = _guildService.GetUsersInRole(profession.Id).ToList();

            return users.Any() ? new Response(string.Join("\n", users.Select(x => x.GetUsername()))) : Responses.Noone;
        }

        public Response Profession(IContext context)
        {
            var profession = context.MentionedRoles.FirstOrDefault();
            if (profession == null)
                return Responses.MissingProfession;

            if (!_settings.FirstOrDefault().Professions.Contains(profession.Id))
                return Responses.Unauthorized;

            if (_guildService.GetUsersInRole(profession.Id).Any(x => x.Id == context.User.Id))
            {
                _guildService.RemoveRole(context.User.Id, profession.Id);
                return Responses.Removed.Params(profession.Name);
            }

            _guildService.AddRole(context.User.Id, profession.Id);
            return Responses.Assigned.Params(profession.Name);
        }
    }
}