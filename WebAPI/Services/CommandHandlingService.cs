﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using WebAPI.Database;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class CommandHandlingService
    {
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly Func<SocketGuild, IGuildService> _guildServiceFactory;
        private readonly Func<string, IRepository<Setting>> _settingsFactory;
        private readonly ILogger<CommandHandlingService> _logger;
        private IServiceProvider _provider;

        public CommandHandlingService(IServiceProvider provider, 
            DiscordSocketClient discord, 
            CommandService commands, 
            Func<SocketGuild, IGuildService> guildServiceFactory, 
            Func<string, IRepository<Setting>> settingsFactory,
            ILogger<CommandHandlingService> logger
            )
        {
            _discord = discord;
            _commands = commands;
            _guildServiceFactory = guildServiceFactory;
            _settingsFactory = settingsFactory;
            _logger = logger;
            _provider = provider;

            _discord.MessageReceived += MessageReceived;
        }

        public async Task InitializeAsync(IServiceProvider provider)
        {
            _provider = provider;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
            // Add additional initialization code here...
        }

        private async Task MessageReceived(SocketMessage rawMessage)
        {
            // Ignore system messages and messages from bots
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            if (message.Channel is SocketGuildChannel channel)
            {
                try
                {
                    var guildService = _guildServiceFactory(channel.Guild);
                    if (guildService.IsReady())
                    {
                        var argPos = 0;
                        var settings = _settingsFactory(guildService.GetDbName());
                        if (!message.HasStringPrefix("s!", ref argPos) && !message.HasStringPrefix(settings.GetAll().First().Prefix, ref argPos)) return;

                        var context = new SocketCommandContext(_discord, message);
                        var result = await _commands.ExecuteAsync(context, argPos, _provider);

                        if (result.Error.HasValue &&
                            result.Error.Value != CommandError.UnknownCommand)
                        {
                            await context.Channel.SendMessageAsync(result.ToString());
                            _logger.LogError(result.ErrorReason);
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "COULD NOT RECEIVE GUILD MESSAGE");
                    throw;
                }
            }
        }
    }
}