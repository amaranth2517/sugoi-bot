﻿namespace WebAPI.Services
{
    public interface IDatabaseService
    {
        void DropCollection(string name);
    }
}