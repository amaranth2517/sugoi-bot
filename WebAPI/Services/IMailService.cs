using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services
{
    public interface IMailService
    {
        Task<bool> SendAsync(string subject, string body, List<Attachment> attachments);
    }
}