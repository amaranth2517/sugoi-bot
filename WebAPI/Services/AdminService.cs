﻿using System;
using System.Linq;
using MongoDB.Bson;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class AdminService
    {
        private readonly IRepository<Setting> _settings;
        private readonly IRepository<Currency> _currencies;
        private readonly IDatabaseService _databaseService;
        private readonly IGuildService _guildService;

        public AdminService(IGuildService guildService, IRepository<Setting> settings, IRepository<Currency> currencies, IDatabaseService databaseService)
        {
            _settings = settings;
            _currencies = currencies;
            _databaseService = databaseService;
            _guildService = guildService;
        }

        public Response Quester(IContext context)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var setting = _settings.FirstOrDefault();
            bool success;
            if (setting == null)
            {
                if (!context.MentionedRoles.Any())
                    return Responses.MissingProfession;

                var roleId = context.MentionedRoles.First().Id;
                setting = new Setting {Quester = roleId};
                success = _settings.Add(setting);
            }
            else
            {
                if (!context.MentionedRoles.Any())
                    return new Response(_guildService.GetRole(setting.Quester).Mention);

                var roleId = context.MentionedRoles.First().Id;
                setting.Quester = roleId;
                success = _settings.Update(setting);
            }

            return success ? Responses.QuesterSet : Responses.DatabaseError;
        }

        private bool Authorized(IContext context)
        {
            return _guildService.IsUserAdmin(context.User.Id);
        }

        public Response Banker(IContext context)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var setting = _settings.FirstOrDefault();
            bool success;
            if (setting == null)
            {
                if (!context.MentionedRoles.Any())
                    return Responses.MissingProfession;

                var roleId = context.MentionedRoles.First().Id;
                setting = new Setting {Banker = roleId};
                success = _settings.Add(setting);
            }
            else
            {
                if (!context.MentionedRoles.Any())
                    return new Response(_guildService.GetRole(setting.Banker).Mention);

                var roleId = context.MentionedRoles.First().Id;
                setting.Banker = roleId;
                success = _settings.Update(setting);
            }

            return success ? Responses.BankerSet : Responses.DatabaseError;
        }

        public Response Prefix(IContext context, string prefix)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var setting = _settings.FirstOrDefault();
            bool success;
            if (setting == null)
            {
                setting = new Setting {Prefix = prefix};
                success = _settings.Add(setting);
            }
            else
            {
                if (string.IsNullOrEmpty(prefix))
                    return new Response(setting.Prefix);

                setting.Prefix = prefix;
                success = _settings.Update(setting);
            }

            return success ? Responses.PrefixSet : Responses.DatabaseError;
        }

        public Response Professions(IContext context)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            // todo: can be null if role is removed
            if (!context.MentionedRoles.Any())
                return new Response(string.Join(", ", _settings.FirstOrDefault()?.Professions.Select(x => _guildService.GetRole(x).Mention)));

            var professions = context.MentionedRoles.Select(x => x.Id).ToList();
            var setting = _settings.FirstOrDefault();
            bool success;

            if (setting == null)
            {
                setting = new Setting {Professions = professions};
                success = _settings.Add(setting);
            }
            else
            {
                setting.Professions = professions;
                success = _settings.Update(setting);
            }

            return success ? Responses.ProfessionsSet : Responses.DatabaseError;
        }

        public Response AddCurrency(IContext context, string slug, string description)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            // todo: check duplicate slug

            return _currencies.Add(new Currency {Description = description, Slug = slug, Professions = context.MentionedRoles.Select(x => x.Id).ToList()})
                ? Responses.CurrencyAdded
                : Responses.DatabaseError;
        }

        public Response UpdateCurrency(IContext context, string slug, string newSlug, string description)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var currency = _currencies.GetBySlug(slug);
            if (currency == null)
                return Responses.NotFound;

            currency.Slug = newSlug;
            currency.Description = description;
            currency.Professions = context.MentionedRoles.Select(x => x.Id).ToList();

            return _currencies.Update(currency)
                ? Responses.CurrencyUpdated
                : Responses.DatabaseError;
        }

        public Response WipeDB(IContext context, string token, DateTime now)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var settings = _settings.FirstOrDefault();
            if (settings == null)
                return Responses.Unauthorized;

            if (settings.WipeToken == null || (now - settings.WipeAt).TotalMinutes >= 2)
            {
                var wipeToken = ObjectId.GenerateNewId().ToString();
                settings.WipeAt = now;
                settings.WipeToken = wipeToken;
                _settings.Update(settings);

                return Responses.WriteTokenToConfirm.Params(wipeToken);
            }

            if (token == settings.WipeToken)
            {
                // todo: mb use reflection for names?
                _databaseService.DropCollection("Completion");
                _databaseService.DropCollection("Reward");
                _databaseService.DropCollection("Request");

                settings.WipeToken = null;
                _settings.Update(settings);

                return Responses.DatabaseWiped;
            }

            return Responses.Unauthorized;
        }

        public Response Channel(IContext context)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var channel = context.MentionedChannels.FirstOrDefault();
            if (channel == null)
                return Responses.NotFound;

            var setting = _settings.FirstOrDefault();
            bool success;
            if (setting == null)
            {
                setting = new Setting {ChannelId = channel.Id};
                success = _settings.Add(setting);
            }
            else
            {
                setting.ChannelId = channel.Id;
                success = _settings.Update(setting);
            }

            return success ? Responses.ChannelSet : Responses.DatabaseError;
        }

        public Response DeleteCurrency(IContext context, string slug)
        {
            if (!Authorized(context))
                return Responses.Unauthorized;

            var currency = _currencies.GetBySlug(slug);
            if (currency == null)
                return Responses.NotFound;

            return _currencies.Delete(currency.Id)
                ? Responses.CurrencyDeleted
                : Responses.DatabaseError;
        }
    }
}