﻿using MongoDB.Driver;
using WebAPI.Database;

namespace WebAPI.Services
{
    public class DatabaseService : IDatabaseService
    {
        private readonly IMongoDatabase _database;

        public DatabaseService(MongoContext context, string dbName)
        {
            _database = context.Client.GetDatabase(dbName);
        }

        public void DropCollection(string name)
        {
            _database.DropCollection(name);
        }
    }
}