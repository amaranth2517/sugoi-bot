﻿using System.Collections.Generic;
using Discord;

namespace WebAPI.Services
{
    public interface IGuildService
    {
        IRole GetRole(ulong professionId);
        bool IsUserInRole(IUser user, ulong roleId);
        IUser GetUser(ulong userId);
        bool IsReady();
        string GetDbName();
        IEnumerable<IUser> GetUsersInRole(ulong professionId);
        bool RemoveRole(ulong userId, ulong professionId);
        bool AddRole(ulong userId, ulong professionId);
        bool IsUserAdmin(ulong userId);
    }
}