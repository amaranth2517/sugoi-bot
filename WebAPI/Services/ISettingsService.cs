﻿namespace WebAPI.Services
{
    public interface ISettingsService
    {
        string Secret { get; }
        string ConnectionString { get; }
        string DbPassword { get; }
        string MailServiceApiKey { get; }
        ulong AmatalkId { get; }
        string MaintenanceSecret { get; set; }
        ulong ReleaseChannelId { get; set; }
        ulong AlphaChannelId { get; set; }
        ulong BetaChannelId { get; set; }
    }
}