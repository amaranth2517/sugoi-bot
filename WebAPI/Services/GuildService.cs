﻿using System.Collections.Generic;
using System.Linq;
using System.Security;
using Discord;
using Discord.WebSocket;
using Microsoft.AspNetCore.Hosting;
using NLog;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class GuildService : IGuildService
    {
        private readonly Logger _logger;
        private readonly SocketGuild _guild;

        public GuildService(IHostingEnvironment env, SocketGuild guild, ISettingsService settingsService, Logger logger)
        {
            _logger = logger;
            switch (env.EnvironmentName)
            {
                case Environments.Development:
                    _guild = guild.Id == settingsService.AmatalkId ? guild : null;
                    break;
                case Environments.Production:
                    _guild = guild.Id == settingsService.AmatalkId ? null : guild;
                    break;
                default:
                    throw new SecurityException("Unsupported environment");
            }

//            ListIds();
        }

        // Make sure production server does not handle messages from Amatalk, while dev server handles messages only from Amatalk.
        public bool IsReady()
        {
            return _guild != null;
        }

        // ReSharper disable once UnusedMember.Local
        private void ListIds()
        {
           _logger.Info("Guild: " + _guild.Name);

            foreach (var user in _guild.Users)
            {
               _logger.Info(user.Nickname + " " + user.Id);
            }

            foreach (var role in _guild.Roles)
            {
               _logger.Info(role.Name + " " + role.Id);
            }
        }

        public IEnumerable<IUser> GetUsersInRole(ulong professionId)
        {
            return _guild.Users.Where(x => x.Roles.Any(y => y.Id == professionId));
        }

        public bool RemoveRole(ulong userId, ulong professionId)
        {
            var role = _guild.Roles.FirstOrDefault(x => x.Id == professionId);
            if (role == null)
                return false;

            var user = _guild.GetUser(userId);

            if (user.Roles.All(x => x.Id != role.Id))
                return false;

            var awaiter = user.RemoveRoleAsync(role).GetAwaiter();
            awaiter.GetResult();

            return awaiter.IsCompleted;
        }

        public bool AddRole(ulong userId, ulong professionId)
        {
            var role = _guild.Roles.FirstOrDefault(x => x.Id == professionId);
            if (role == null)
                return false;

            var user = _guild.GetUser(userId);

            if (user.Roles.Any(x => x.Id == role.Id))
                return false;

            var awaiter = user.AddRoleAsync(role).GetAwaiter();
            awaiter.GetResult();

            return awaiter.IsCompleted;
        }

        public bool IsUserAdmin(ulong userId)
        {
            // todo: Hardcoded Amaranth's Id. Refactor to setting.
            return _guild.GetUser(userId).GuildPermissions.Administrator || userId == 101737300118994944;
        }

        public IRole GetRole(ulong professionId)
        {
            return _guild.GetRole(professionId);
        }

        public bool IsUserInRole(IUser user, ulong roleId)
        {
            var role = _guild.Roles.FirstOrDefault(x => x.Id == roleId);
            return role != null && role.Members.Any(x => x.Id == user.Id);
        }

        public IUser GetUser(ulong userId)
        {
            return _guild.GetUser(userId);
        }

        public string GetDbName()
        {
            return _guild.Id.ToString();
        }
    }
}