﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using Attachment = WebAPI.Models.Attachment;

namespace WebAPI.Services
{
    public class SendGridService : IMailService
    {
        private readonly SendGridClient _client;

        public SendGridService(ISettingsService settingsService)
        {
            _client = new SendGridClient(settingsService.MailServiceApiKey);
        }

        public async Task<bool> SendAsync(string subject, string body, List<Attachment> attachments)
        {
            var transmission = new SendGridMessage
            {
                Attachments = attachments.Select(x => new SendGrid.Helpers.Mail.Attachment
                {
                    Content = x.Data,
                    Filename = x.Name,
                    Type = x.Type
                }).ToList(),
                From = new EmailAddress("dagrachev@sem-tech.net"),
                Subject = subject,
                HtmlContent = body,
                Personalizations = new List<Personalization>
                {
                    new Personalization
                    {
                        Tos = new List<EmailAddress>
                        {
                            new EmailAddress("dagrachev@sem-tech.net")
                        }
                    }
                }
            };

            var response = await _client.SendEmailAsync(transmission);

            return IsSuccessStatusCode(response.StatusCode);
        }

        private static bool IsSuccessStatusCode(HttpStatusCode statusCode)
        {
            var asInt = (int) statusCode;

            return asInt >= 200 && asInt <= 299;
        }
    }
}