﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Discord;
using MongoDB.Bson;
using WebAPI.Database;
using WebAPI.Extensions;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class QuestingService
    {
        private readonly IRepository<Quest> _quests;
        private readonly IRepository<Completion> _completions;
        private readonly IRepository<Reward> _rewards;
        private readonly IRepository<Rate> _rates;
        private readonly IRepository<Setting> _settings;
        private readonly IRepository<Currency> _currencies;
        private readonly IRepository<Request> _requests;
        private readonly IGuildService _guildService;

        public QuestingService(IGuildService guildService, IRepository<Quest> quests, IRepository<Completion> completions, IRepository<Reward> rewards,
            IRepository<Rate> rates, IRepository<Setting> settings, IRepository<Currency> currencies, IRepository<Request> requests)
        {
            _quests = quests;
            _completions = completions;
            _rewards = rewards;
            _rates = rates;
            _settings = settings;
            _currencies = currencies;
            _requests = requests;
            _guildService = guildService;
        }

        public Response Quests(string type)
        {
            var allQuests = _quests.GetAll().ToList();
            var response = new ResponseBuilder();

            if (allQuests.Any())
            {
                var currentType = QuestType.None;
                foreach (var quest in allQuests.Where(x => x.Active).OrderBy(x => x.QuestType).ThenBy(x => x.Amount))
                {
                    var builder = new StringBuilder();
                    if (currentType != quest.QuestType)
                    {
                        currentType = quest.QuestType;
                        builder.AppendLine($"**{currentType.Print()}**");
                    }

                    var techInfo = type == "show-ids" ? " (" + quest.Id + ", " + quest.Slug + ")" : "";
                    builder.AppendLine($"\"{quest.Name}\"{techInfo}");
                    builder.AppendLine($"{quest.Description}");
                    builder.AppendLine($"Reward: {quest.Amount:0.##} {quest.PrintCurrency(_currencies)}");
                    response.AppendLine(builder.ToString());
                }
            }
            else
            {
                response.AppendLine("We have no active quests.");
            }

            return new Response(response.ToString());
        }

        public Response AddQuest(IContext context, string[] args)
        {
            if (!AuthorizedQuester(context))
                return Responses.Unauthorized;

            var parameters = args.ToList();

            var name = GetValue(parameters, "n");
            var slug = GenerateSlug(name);

            var collidingQuest = _quests.GetBySlug(slug);
            if (collidingQuest != null)
                return Responses.DuplicateSlug.Params(slug, collidingQuest.Name);

            var description = GetValue(parameters, "d");

            if (!float.TryParse(GetValue(parameters, "a")?.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out var amount))
                return Responses.AmountIsNotNumber;

            var currency = _currencies.GetBySlug(GetValue(parameters, "c"));
            if (currency == null)
                return Responses.InvalidRewardType;

            if (!Enum<QuestType>.TryParse(GetValue(parameters, "t"), out var questType))
                return Responses.InvalidQuestType;

            _quests.Add(new Quest
            {
                Name = name,
                Slug = slug,
                QuestType = questType,
                Description = description,
                Amount = amount,
                CurrencyId = currency.Id,
                Active = true
            });

            return Responses.QuestAdded.Params(name, slug);
        }

        private string GenerateSlug(string name)
        {
            return string.Join("", name.Split(' ').Select(x => x.ElementAt(0))).ToLower();
        }

        private string GetValue(List<string> args, string parameter)
        {
            var parameters = new List<string> {"n", "d", "a", "c", "t"};
            var index = args.FindIndex(x => x == Norm(parameter));
            if (index == -1)
                throw new ArgumentException($"{Norm(parameter)} argument is missing");

            var value = new List<string>();
            for (var i = index + 1; i < args.Count && parameters.All(x => Norm(x) != args[i]); i++)
            {
                value.Add(args[i]);
            }

            return string.Join(" ", value);
        }

        private static string Norm(string parameter)
        {
            return "-" + parameter;
        }

        public Response UpdateQuest(IContext context, string[] args)
        {
            if (!AuthorizedQuester(context))
                return Responses.Unauthorized;

            var idOrSlug = args[0]?.ToLower();
            var quest = _quests.GetBySlug(idOrSlug);

            if (quest == null)
                return Responses.NotFound;

            var parameters = args.ToList();

            try
            {
                quest.Name = GetValue(parameters, "n");
            }
            catch
            {
                // ignored
            }

            try
            {
                quest.Description = GetValue(parameters, "d");
            }
            catch
            {
                // ignored
            }

            try
            {
                if (!float.TryParse(GetValue(parameters, "a")?.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out var amount))
                    return Responses.AmountIsNotNumber;

                quest.Amount = amount;
            }
            catch
            {
                // ignored
            }

            try
            {
                var currencySlug = GetValue(parameters, "c");
                if (quest.Version == 2)
                {
                    var currency = _currencies.GetBySlug(currencySlug);
                    if (currency == null)
                        return Responses.InvalidRewardType;

                    quest.CurrencyId = currency.Id;
                }
                else
                {
                    if (!Enum<RewardType>.TryParse(currencySlug, out var rewardType))
                        return Responses.InvalidRewardType;

                    quest.RewardType = rewardType;
                }
            }
            catch
            {
                // ignored
            }

            try
            {
                if (!Enum<QuestType>.TryParse(GetValue(parameters, "t"), out var questType))
                    return Responses.InvalidQuestType;

                quest.QuestType = questType;
            }
            catch
            {
                // ignored
            }

            return _quests.Update(quest) ? Responses.QuestUpdated.Params(quest.Name) : Responses.DatabaseError;
        }

        private bool AuthorizedQuester(IContext context)
        {
            return _guildService.IsUserInRole(context.User, _settings.FirstOrDefault().Quester);
        }

        private bool AuthorizedBanker(IContext context)
        {
            return _guildService.IsUserInRole(context.User, _settings.FirstOrDefault().Banker);
        }

        public Response DisableQuest(IContext context, string idOrSlug)
        {
            if (!AuthorizedQuester(context))
                return Responses.Unauthorized;

            var quest = _quests.GetBySlug(idOrSlug);

            if (quest == null)
                return Responses.NotFound;

            quest.Active = false;

            return _quests.Update(quest) ? Responses.QuestDisabled.Params(quest.Name) : Responses.DatabaseError;
        }

        public Response EnableQuest(IContext context, string idOrSlug)
        {
            if (!AuthorizedQuester(context))
                return Responses.Unauthorized;

            var quest = _quests.GetBySlug(idOrSlug);

            if (quest == null)
                return Responses.NotFound;

            quest.Active = true;

            return _quests.Update(quest) ? Responses.QuestEnabled.Params(quest.Name) : Responses.DatabaseError;
        }

        public Response AddTitle(IContext context, string questIdOrSlug, int completions, params string[] name)
        {
            if (!AuthorizedQuester(context))
                return Responses.Unauthorized;

            var quest = _quests.GetBySlug(questIdOrSlug);

            if (quest == null)
                return Responses.NotFound;

            if (quest.Titles == null)
                quest.Titles = new List<Title>();

            var title = string.Join(" ", name);
            quest.Titles.Add(new Title {Completions = completions, Name = title});
            quest.Titles.Sort((a, b) => a.Completions.CompareTo(b.Completions));

            return _quests.Update(quest) ? Responses.TitleAdded.Params(title, quest.Name) : Responses.DatabaseError;
        }

        public Response Turn(IContext context, string questIdOrSlug, string amount, string rewardSlug, params string[] args)
        {
            if (!AuthorizedBanker(context))
                return Responses.Unauthorized;

            if (!context.MentionedUsers.Any())
                return Responses.MissingPlayers;

            var quest = _quests.GetBySlug(questIdOrSlug);
            if (quest == null)
                return Responses.NotFound;

            if (!float.TryParse(amount?.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out var quantity))
            {
                quantity = quest.Amount;
            }

            quantity /= context.MentionedUsers.Count();

            var currency = quest.Version == 2 ? _currencies.Get(quest.CurrencyId) : _currencies.GetBySlug(quest.RewardType.Sign());

            if (!string.IsNullOrEmpty(rewardSlug))
            {
                var bySlug = _currencies.GetBySlug(rewardSlug);
                if (bySlug != null)
                    currency = bySlug;
            }

            //todo: it needs to be a single transaction
            foreach (var user in context.MentionedUsers)
            {
                SaveCompletion(context, user, quest, quantity, currency);
                UpdateRewards(user, quantity, currency);
            }

            return Responses.TurnedIn.Params(string.Join(", ", context.MentionedUsers.Select(x => x.GetUsername())), quantity, currency.Description);
        }

        private void SaveCompletion(IContext context, IUser user, Quest quest, float quantity, Currency currency)
        {
            _completions.Add(new Completion
            {
                PlayerId = user.Id,
                QuestId = quest.Id,
                Amount = quantity,
                CurrencyId = currency.Id,
                HelpersIds = context.MentionedUsers.Where(x => x != user).Select(x => x.Id).ToList(),
                Screenshot = context.Attachments.FirstOrDefault()?.Url
            });
        }

        private void UpdateRewards(IUser user, float amount, Currency currency)
        {
            var record = _rewards.GetAll().FirstOrDefault(x => x.PlayerId == user.Id);
            if (record == null)
            {
                var reward = new Reward
                {
                    PlayerId = user.Id,
                    Points = new Dictionary<string, float> {{currency.Id.ToString(), amount}}
                };

                _rewards.Add(reward);
            }
            else
            {
                record.UpdateRewards(currency, amount, _currencies);
                _rewards.Update(record);
            }
        }

        public Response Rewards(IContext context, string player)
        {
            var user = context.MentionedOrCurrentUser;
            var record = _rewards.GetAll().FirstOrDefault(x => x.PlayerId == user.Id);
            if (record == null)
                return Responses.NoRewards.Params(user.GetUsername());

            var response = new StringBuilder();
            response.Append($"{user.GetUsername()} has");

            const double tolerance = 1e-10;

            if (record.Version == 2)
            {
                foreach (var (key, value) in record.Points)
                {
                    if (Math.Abs(value) > tolerance)
                        response.Append($" {value:0.##} {_currencies.Get(ObjectId.Parse(key)).Description};");
                }
            }
            else
            {
                if (Math.Abs(record.GearPoints) > tolerance)
                    response.Append($" {record.GearPoints:0.##} {RewardType.GearPoints.Print()};");

                if (Math.Abs(record.VesselPoints) > tolerance)
                    response.Append($" {record.VesselPoints:0.##} {RewardType.VesselPoints.Print()};");

                if (Math.Abs(record.JewelryPoints) > tolerance)
                    response.Append($" {record.JewelryPoints:0.##} {RewardType.JewelryPoints.Print()};");

                if (Math.Abs(record.Coins) > tolerance)
                    response.Append($" {record.Coins:0.##} {RewardType.Coins.Print()};");
            }

            return new Response(response.ToString());
        }

        public Response Completions(IContext context, string playerOrQuest)
        {
            var response = new ResponseBuilder();

            var quest = _quests.GetBySlug(playerOrQuest);
            if (quest == null)
            {
                var user = context.MentionedOrCurrentUser;
                var playerCompletions = _completions.GetByPlayer(user.Id).ToList();

                if (playerCompletions.Any())
                {
                    response.AppendLine($"{user.GetUsername()}'s quest completions:");
                    foreach (var completion in playerCompletions.OrderByDescending(x => x.CreatedAt))
                    {
                        quest = _quests.Get(completion.QuestId);
                        var helpers = completion.HelpersIds.Select(x => _guildService.GetUser(x)?.GetUsername()).ToList();
                        var helpersMessage = helpers.Any() ? string.Join(", ", helpers) + " were in group. " : "";
                        response.AppendLine(
                            $"{completion.CreatedAt.ToShortDateString()}: {completion.Amount:0.##} {_currencies.Get(completion.CurrencyId)?.Description}, {quest?.Name} {helpersMessage}{completion.Screenshot ?? "No screenshot."}");
                    }
                }
                else
                {
                    response.AppendLine("Player did not complete any quest.");
                }
            }
            else
            {
                var questCompletions = _completions.GetAll().Where(x => x.QuestId == quest.Id).ToList();

                if (questCompletions.Any())
                {
                    response.AppendLine($"{quest.Name} completions:");
                    foreach (var completion in questCompletions.OrderByDescending(x => x.CreatedAt))
                    {
                        var playerIds = new List<ulong> {completion.PlayerId};
                        playerIds.AddRange(completion.HelpersIds);
                        var players = playerIds.Select(x => _guildService.GetUser(x)?.GetUsername()).ToList();
                        var playersMessage = players.Any() ? string.Join(", ", players) + ". " : "";
                        response.AppendLine(
                            $"{completion.CreatedAt.ToShortDateString()}: {completion.Amount:0.##} {_currencies.Get(completion.CurrencyId)?.Description}, {playersMessage}{completion.Screenshot ?? "No screenshot."}");
                    }
                }
                else
                {
                    response.AppendLine($"Noone completed {quest.Name}.");
                }
            }

            return new Response(response.ToString());
        }

        public Response Titles(IContext context, string player)
        {
            var playerCompletions = _completions.GetByPlayer(context.MentionedOrCurrentUser.Id).ToList();

            if (playerCompletions.Any())
            {
                var map = new Dictionary<ObjectId, float>();
                foreach (var completion in playerCompletions)
                {
                    if (!map.ContainsKey(completion.QuestId))
                        map.Add(completion.QuestId, 0);

                    map[completion.QuestId] += completion.Amount;
                }

                var titles = new List<string>();

                foreach (var m in map)
                {
                    var quest = _quests.Get(m.Key);
                    if (quest != null && quest.Titles?.Any() == true)
                    {
                        var title = quest.Titles.LastOrDefault(x => x.Completions <= m.Value);
                        if (title != null)
                            titles.Add(title.Name);
                    }
                }

                if (titles.Any())
                    return new Response(string.Join(", ", titles));
            }

            return Responses.NoTitles.Params(context.MentionedOrCurrentUser.GetUsername());
        }

        public Response Rates(IContext context)
        {
            var result = new StringBuilder();
            foreach (var rate in _rates.GetAll())
            {
                result.AppendLine(rate.Version == 2
                    ? $"{rate.SourceAmount} {_currencies.Get(rate.SourceCurrencyId).Description} = {rate.TargetAmount} {_currencies.Get(rate.TargetCurrencyId).Description}"
                    : $"{rate.SourceAmount} {rate.SourceCurrency.Print()} = {rate.TargetAmount} {rate.TargetCurrency.Print()}");
            }

            return new Response(result.ToString());
        }

        public Response Rate(IContext context, float amount1, string currency1, float amount2, string currency2)
        {
            if (!AuthorizedBanker(context))
                return Responses.Unauthorized;

            var sourceCurrency = _currencies.GetBySlug(currency1);
            if (sourceCurrency == null)
                return Responses.UnknownCurrency.Params(currency1);

            var targetCurrency = _currencies.GetBySlug(currency2);
            if (targetCurrency == null)
                return Responses.UnknownCurrency.Params(currency2);

            bool result;
            var rate = _rates.GetAll().FirstOrDefault(x => x.SourceCurrencyId == sourceCurrency.Id && x.TargetCurrencyId == targetCurrency.Id);
            if (rate == null)
            {
                rate = new Rate
                {
                    SourceCurrencyId = sourceCurrency.Id,
                    TargetCurrencyId = targetCurrency.Id,
                    SourceAmount = amount1,
                    TargetAmount = amount2
                };

                result = _rates.Add(rate);
            }
            else
            {
                rate.SourceCurrencyId = sourceCurrency.Id;
                rate.TargetCurrencyId = targetCurrency.Id;
                rate.SourceAmount = amount1;
                rate.TargetAmount = amount2;

                result = _rates.Update(rate);
            }

            return result ? Responses.RateSet : Responses.DatabaseError;
        }

        public Response Convert(IContext context, string amountRaw, string currency1, string currency2)
        {
            if (!float.TryParse(amountRaw.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out var amount))
                return Responses.AmountIsNotNumber;

            if (amount <= 0)
                return Responses.Unauthorized;

            var sourceCurrency = _currencies.GetBySlug(currency1);
            if (sourceCurrency == null)
                return Responses.UnknownCurrency.Params(currency1);

            var targetCurrency = _currencies.GetBySlug(currency2);
            if (targetCurrency == null)
                return Responses.UnknownCurrency.Params(currency2);

            var rate = _rates.GetAll().FirstOrDefault(x => x.SourceCurrencyId == sourceCurrency.Id && x.TargetCurrencyId == targetCurrency.Id);
            if (rate == null)
                return Responses.ExchangeNotAllowed;

            var record = _rewards.GetByPlayer(context.User.Id).FirstOrDefault();
            if (record == null)
                return Responses.NotEnoughCurrency;

            var availableAmount = record.HasRewards(sourceCurrency, _currencies) ? record.Points[sourceCurrency.Id.ToString()] : 0;
            if (availableAmount < amount)
                return Responses.NotEnoughCurrency;

            record.UpdateRewards(sourceCurrency, -amount, _currencies);
            var exchangedAmount = amount / rate.SourceAmount * rate.TargetAmount;
            record.UpdateRewards(targetCurrency, exchangedAmount, _currencies);

            return _rewards.Update(record) ? Responses.Converted.Params(amount, sourceCurrency.Description, exchangedAmount, targetCurrency.Description) : Responses.DatabaseError;
        }

        public Response Total(IContext context)
        {
            if (!AuthorizedBanker(context))
                return Responses.Unauthorized;

            var result = new Dictionary<string, float>();
            foreach (var reward in _rewards.GetAll())
            {
                foreach (var (key, value) in reward.Points)
                {
                    if (result.ContainsKey(key))
                        result[key] += value;
                    else
                        result.Add(key, value);
                }
            }

            foreach (var request in _requests.GetAll().Where(x => x.State != CraftingState.Crafted))
            {
                var key = request.CurrencyId.ToString();
                if (result.ContainsKey(key))
                    result[key] += request.Amount;
                else
                    result.Add(key, request.Amount);
            }

            return new Response(string.Join(", ", result.Select(x => x.Value + " " + _currencies.Get(ObjectId.Parse(x.Key))?.Description)));
        }
    }
}