﻿using System;

namespace WebAPI.Services
{
    public class SettingsService : ISettingsService
    {
        public string Secret { get; }
        public string ConnectionString { get; }
        public string DbPassword { get; }
        public string MailServiceApiKey { get; }
        public ulong AmatalkId { get; }
        public string MaintenanceSecret { get; set; }
        public ulong ReleaseChannelId { get; set; }
        public ulong AlphaChannelId { get; set; }
        public ulong BetaChannelId { get; set; }

        public SettingsService()
        {
            ConnectionString = Environment.GetEnvironmentVariable("CONNECTION_STRING");
            Secret = Environment.GetEnvironmentVariable("SECRET");
            MailServiceApiKey = Environment.GetEnvironmentVariable("SPARKPOST_API_KEY");
            DbPassword = Environment.GetEnvironmentVariable("DB_PASSWORD");
            AmatalkId = ulong.Parse(Environment.GetEnvironmentVariable("DEV_GUILD_ID") ?? throw new InvalidOperationException("AmatalkId is not set"));
            MaintenanceSecret = Environment.GetEnvironmentVariable("MAINTENANCE_SECRET");
            ReleaseChannelId = ulong.Parse(Environment.GetEnvironmentVariable("RELEASE_CHANNEL_ID") ?? throw new InvalidOperationException("ReleaseChannelId is not set"));
            BetaChannelId = ulong.Parse(Environment.GetEnvironmentVariable("BETA_CHANNEL_ID") ?? throw new InvalidOperationException("BetaChannelId is not set"));
            AlphaChannelId = ulong.Parse(Environment.GetEnvironmentVariable("ALPHA_CHANNEL_ID") ?? throw new InvalidOperationException("AlphaChannelId is not set"));
        }
    }
}